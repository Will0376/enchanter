package ru.will0376.enchanter.client;

import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.will0376.enchanter.common.ModBlock;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;
import ru.will0376.enchanter.server.ServerProxy;

public class ClientProxy extends ServerProxy {

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);

	}

	@Override
	public void init(FMLInitializationEvent event) {
		super.init(event);
		ModBlock.initModels();
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
		TileEntityRendererDispatcher.instance.renderers.put(TEEnchanter.class, new TileEntityEnchantmentTableRendererReplaced());
	}
}
