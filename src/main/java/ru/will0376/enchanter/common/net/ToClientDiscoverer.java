package ru.will0376.enchanter.common.net;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.client.guicontainer.DiscovererGui;
import ru.will0376.enchanter.server.PlayerPojo;

import java.awt.*;

public class ToClientDiscoverer implements IMessage {
	Action action;
	String text;

	public ToClientDiscoverer() {
	}

	public ToClientDiscoverer(Action action) {
		this.action = action;
		this.text = "";
	}

	public ToClientDiscoverer(Action action, String text) {
		this.action = action;
		this.text = text;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		action = Action.getById(buf.readInt());
		text = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
		ByteBufUtils.writeUTF8String(buf, text);
	}

	public enum Action {
		OpenGui,
		SetPojo,
		ErrorText,
		Success,
		Service;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToClientDiscoverer, IMessage> {

		@Override
		public ToClientDiscoverer onMessage(ToClientDiscoverer message, MessageContext ctx) {
			Invoke.client(() -> onMessage1(message, ctx));
			return null;
		}

		@SideOnly(Side.CLIENT)
		public void onMessage1(ToClientDiscoverer message, MessageContext ctx) {
			if (FMLCommonHandler.instance().getSide().isClient()) {
				switch (message.action) {
					case OpenGui:
						Minecraft.getMinecraft()
								.addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new DiscovererGui()));
						break;
					case SetPojo:
						PlayerPojo.setClientpojo(message.text);
						break;
					case Service:
//						Minecraft.getMinecraft().addScheduledTask(() -> {
//							ArrayList<IRecipe> recipes = Lists.newArrayList(ForgeRegistries.RECIPES.getValuesCollection());
//							for (int i = 0; i <= 11; i++) {
//								System.out.println("test: " + i);
//								ItemStack removed = new ItemStack(ModBlock.ball, 1, i);
//								for (IRecipe tmp : recipes) {
//									if (tmp.getRecipeOutput().getItem() != Items.AIR
//											&& removed.getItem() == tmp.getRecipeOutput().getItem()
//											&& tmp.getRecipeOutput().getMetadata() == removed.getMetadata()) {
//										System.out.println("recipe: " + i + " here....");
//									}
//								}
//							}
//						});
						break;
				}
				if (Minecraft.getMinecraft().currentScreen instanceof DiscovererGui) {
					DiscovererGui dis = (DiscovererGui) Minecraft.getMinecraft().currentScreen;
					switch (message.action) {
						case Success:
							dis.color = Color.GREEN;
							dis.tickText = 20 * 6;
							dis.lastText = message.text;
							Minecraft.getMinecraft().displayGuiScreen(null);
							break;
						case ErrorText:
							dis.color = Color.RED;
							dis.tickText = 20 * 6;
							dis.lastText = message.text;
							break;
					}
				}
			}
		}
	}
}
