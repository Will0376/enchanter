package ru.will0376.enchanter.common.container.slot;

import net.minecraft.item.*;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ToolSlot extends SlotItemHandler {

	public ToolSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		Item item = stack.getItem();
		return item instanceof ItemBow || item instanceof ItemArmor || item instanceof ItemPickaxe || item instanceof ItemSword || item instanceof ItemAxe || item instanceof ItemSpade || item instanceof ItemFishFood || item instanceof ItemFlintAndSteel || item instanceof ItemShield || item instanceof ItemHoe;
	}
}
