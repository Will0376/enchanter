package ru.will0376.enchanter.common;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IRecipeFactory;
import ru.will0376.enchanter.Enchanter;

public class CraftRegister {

	public static void registerRecipes() {
		CraftingHelper.register(new ResourceLocation(Enchanter.MOD_ID, "ball"), (IRecipeFactory) (context, json) -> CraftingHelper
				.getRecipe(json, context));
	}
}
