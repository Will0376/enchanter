package ru.will0376.enchanter.common.net;

import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import ru.will0376.enchanter.Enchanter;

public class Net {
	private static final SimpleNetworkWrapper network = new SimpleNetworkWrapper(Enchanter.MOD_ID);

	public static void init() {
		int i = 1;
		network.registerMessage(new ToClientEnchanter.Handler(), ToClientEnchanter.class, i++, Side.CLIENT);
		network.registerMessage(new ToServerEnchanter.Handler(), ToServerEnchanter.class, i++, Side.SERVER);
		network.registerMessage(new ToServerAnvil.Handler(), ToServerAnvil.class, i++, Side.SERVER);
		network.registerMessage(new ToClientAnvil.Handler(), ToClientAnvil.class, i++, Side.CLIENT);
		network.registerMessage(new ToClientDiscoverer.Handler(), ToClientDiscoverer.class, i++, Side.CLIENT);
		network.registerMessage(new ToServerDiscoverer.Handler(), ToServerDiscoverer.class, i++, Side.SERVER);
	}

	public static SimpleNetworkWrapper get() {
		return network;
	}
}
