package ru.will0376.enchanter.client.guicontainer;

import austeretony.oxygen_core.client.OxygenManagerClient;
import austeretony.oxygen_core.client.api.WatcherHelperClient;
import austeretony.oxygen_core.client.currency.CurrencyProperties;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.GuiScrollingList;
import org.lwjgl.input.Keyboard;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.client.GuiButtonSome;
import ru.will0376.enchanter.client.GuiHelper;
import ru.will0376.enchanter.common.Enums;
import ru.will0376.enchanter.common.container.AnvilContainer;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToServerAnvil;
import ru.will0376.enchanter.common.net.ToServerEnchanter;
import ru.will0376.enchanter.common.tileentity.TEAnvil;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import static ru.will0376.enchanter.client.guicontainer.AnvilGuiContainer.Page.*;
import static ru.will0376.enchanter.common.Enums.*;
import static ru.will0376.enchanter.common.net.ToServerEnchanter.nbtEnchantLvlConstant;

public class AnvilGuiContainer extends GuiContainer {
	public static Page page;
	private static final ResourceLocation background = new ResourceLocation(Enchanter.MOD_ID, "textures/gui/anvilgui.png");
	public int selectedId = -1;
	public HashMap<Integer, String> map;
	TEAnvil tileEntity;
	ScrollText list;
	ScaledResolution resolution;
	GuiTextField textField;
	GuiButton renameButton, repairbutton;
	GuiButtonSome reechant, rename, repair;

	public AnvilGuiContainer(TEAnvil tileEntity, AnvilContainer container) {
		super(container);
		this.tileEntity = tileEntity;
		mc = Minecraft.getMinecraft();
		resolution = new ScaledResolution(mc);
		if (page == null) page = Reenchant;
	}

	@Override
	public void initGui() {
		super.initGui();
		ItemStack input = tileEntity.getItemStackHandler().getStackInSlot(0);
		switch (page) {
			case Reenchant:
				int top = height / 2 - 88;
				int localW = 110;
				list = new ScrollText(mc, localW + 4, 50, top, top + 100, width / 2 - localW * 2 + 18, 26, width, height, input);
				map = getListByStack(input);
				addButton(new GuiButton(0, width / 2 - 50, height / 2 - 44, 100, 20, I18n.format("anvil.reenchant.button")));
				break;

			case Rename:
				int widthlk = 112;
				textField = new GuiTextField(-1, fontRenderer, width / 2 - widthlk / 2, height / 2 - 72, widthlk, 25);
				if (!input.isEmpty()) textField.setText(input.getDisplayName());
				else textField.setText(" ");
				renameButton = addButton(new GuiButton(1, width / 2 - 50, height / 2 - 44, 100, 20, I18n.format("anvil.rename.button")));
				break;
			case Repair:
				repairbutton = addButton(new GuiButton(2, width / 2 - 50, height / 2 - 44, 100, 20, I18n.format("anvil.repair.button")));
				break;
		}
		reechant = addButton(new GuiButtonSome(10, "textures/gui/button.png", "textures/gui/button-sel.png", width / 2 + 86, height / 2 - 74, 35, 26, Reenchant
				.name()).setSelected(page == Reenchant));
		rename = addButton(new GuiButtonSome(11, "textures/gui/button.png", "textures/gui/button-sel.png", width / 2 + 86, height / 2 - 47, 35, 26, Rename
				.name()).setSelected(page == Rename));
		repair = addButton(new GuiButtonSome(12, "textures/gui/button.png", "textures/gui/button-sel.png", width / 2 + 86, height / 2 - 20, 35, 26, Repair
				.name()).setSelected(page == Repair));
	}

	@Override
	protected void actionPerformed(GuiButton button) {
		switch (button.id) {
			case 0:
				Net.get().sendToServer(new ToServerAnvil(ToServerAnvil.Action.ReEnchant, selectedId + ""));
				break;
			case 1:
				if (textField != null && !textField.getText().isEmpty())
					Net.get().sendToServer(new ToServerAnvil(ToServerAnvil.Action.Rename, textField.getText()));
				break;
			case 2:
				Net.get().sendToServer(new ToServerAnvil(ToServerAnvil.Action.Repair));
				break;
			case 10:
				page = Reenchant;
				mc.displayGuiScreen(this);
				break;
			case 11:
				page = Rename;
				mc.displayGuiScreen(this);
				break;
			case 12:
				page = Repair;
				mc.displayGuiScreen(this);
				break;

		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		try {
			renderEnchants(mouseX, mouseY, partialTicks);
			renderHoveredToolTip(mouseX, mouseY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) {
		try {
			if (keyCode == Keyboard.KEY_ESCAPE) {
				mc.displayGuiScreen(null);
				mc.setIngameFocus();
			} else {
				if (textField != null && page == Rename && textField.isFocused())
					textField.textboxKeyTyped(typedChar, keyCode);
			}
			super.keyTyped(typedChar, keyCode);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void updateScreen() {
		try {
			if (textField != null && page == Rename) textField.updateCursorCounter();
			super.updateScreen();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int state) {
		try {
			if (textField != null && page == Rename) {
				if (textField.isFocused() && state == 1) textField.setText("");
				textField.mouseClicked(mouseX, mouseY, state);
			}
			super.mouseClicked(mouseX, mouseY, state);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void renderEnchants(int mouseX, int mouseY, float partialTicks) {
		ItemStack input = tileEntity.getItemStackHandler().getStackInSlot(0);
		GuiHelper.bindTexture("textures/gui/reenchant.png");
		GuiHelper.cleanRender(width / 2 + 98, height / 2 - 69, 16, 16, 1);
		GuiHelper.bindTexture("textures/gui/rename.png");
		GuiHelper.cleanRender(width / 2 + 98, height / 2 - 42, 16, 16, 1);
		GuiHelper.bindTexture("textures/gui/repair.png");
		GuiHelper.cleanRender(width / 2 + 98, height / 2 - 15, 16, 16, 1);
		reechant.drawHover(mouseX, mouseY);
		rename.drawHover(mouseX, mouseY);
		repair.drawHover(mouseX, mouseY);

		CurrencyProperties prop = OxygenManagerClient.instance().getCurrencyManager().getProperties(Enchanter.index);
		fontRenderer.drawString(String.valueOf(WatcherHelperClient.getLong(prop.getIndex())), width / 2 + 40, height / 2 - 92, Color.YELLOW
				.getRGB(), true);
		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getRenderManager().renderEngine.bindTexture(prop.getIcon());
		GuiHelper.cleanRender(width / 2 + 24, height / 2 - 96, 16, 16, 2);
		GlStateManager.pushMatrix();
		fontRenderer.drawString(page.name() + ":", width / 2 - 80, height / 2 - 92, Color.GREEN.getRGB(), true);
		switch (page) {
			case Reenchant:
				String text = I18n.format("gui.anvil.reenchant.text");
				fontRenderer.drawString(text, width / 2 - fontRenderer.getStringWidth(text) / 2, height / 2 - 72, Color.BLACK
						.getRGB(), false);
				text = map.get(selectedId);
				fontRenderer.drawString(text, width / 2 - fontRenderer.getStringWidth(text) / 2, height / 2 - 60, Color.BLUE
						.getRGB(), false);
				if (input.isEmpty()) clearList();
				else {
					map = getListByStack(input);
					list.setStack(input);
				}
				if (list.getSize() != 0) list.drawScreen(mouseX, mouseY, partialTicks);
				break;
			case Rename:

				if (!input.isEmpty() && textField.getText().equals(" ")) textField.setText(input.getDisplayName());
				textField.drawTextBox();
				renameButton.enabled = !input.isEmpty();
				renameButton.enabled = !input.getDisplayName().equals(textField.getText());
				renameButton.enabled = textField.getText()
						.length() * Enchanter.multiple / 2 <= WatcherHelperClient.getLong(prop.getIndex());

				text = String.valueOf(textField.getText().length() * Enchanter.multiple / 2);
				GlStateManager.popMatrix();
				GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
				mc.getRenderManager().renderEngine.bindTexture(prop.getIcon());
				GuiHelper.cleanRender(width / 2 + fontRenderer.getStringWidth(text) / 2 + 65, height / 2 - 42, 16, 16, 2);
				GlStateManager.pushMatrix();
				fontRenderer.drawString(text, width / 2 - fontRenderer.getStringWidth(text) / 2 + 65, height / 2 - 37, Color.BLUE
						.getRGB(), false);
				break;
			case Repair:
				if (!input.isEmpty()) {
					repairbutton.enabled = input.getItemDamage() * Enchanter.multiple <= WatcherHelperClient.getLong(prop
							.getIndex()) && input.getItemDamage() != 0;

					text = String.valueOf(input.getItemDamage() * Enchanter.multiple);
					GlStateManager.popMatrix();
					GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
					mc.getRenderManager().renderEngine.bindTexture(prop.getIcon());
					GuiHelper.cleanRender(width / 2 - fontRenderer.getStringWidth(text) / 2 + 20 - 2, height / 2 - 65 - 5, 16, 16, 2);
					GlStateManager.pushMatrix();
					fontRenderer.drawString(text, width / 2 - fontRenderer.getStringWidth(text) / 2 - 2, height / 2 - 60 - 5, Color.BLUE
							.getRGB(), false);

				} else {
					repairbutton.enabled = false;
				}
				break;
		}
	}

	private void clearList() {
		list.setStack(ItemStack.EMPTY);
		map.clear();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		mc.getTextureManager().bindTexture(background);
		drawTexturedModalRect(guiLeft + 1, guiTop - 14, 0, 0, xSize, ySize + 14);
	}

	public int getByIndex(int index) {
		int off = 0;
		for (Integer e : map.keySet()) {
			if (index == off) return e;
			off++;
		}
		return -1;
	}

	public HashMap<Integer, String> getListByStack(ItemStack stack) {
		Item item = stack.getItem();
		HashMap<Integer, String> map = new HashMap<>();
		boolean firstEnchant = true;
		if (stack.getTagCompound() != null && stack.hasTagCompound() && stack.getTagCompound()
				.hasKey(nbtEnchantLvlConstant)) firstEnchant = false;

		int curlvl = firstEnchant ? 0 : stack.getTagCompound().getInteger(nbtEnchantLvlConstant);
		NBTTagCompound nbt = stack.getTagCompound() != null ? stack.getTagCompound() : new NBTTagCompound();
		if (item instanceof ItemSword) {
			ArrayList<Integer> collisions = SwordEnchants.getCollisionsIdByLvl(curlvl);
			HashMap<Enums.SwordEnchants, Integer> opened = SwordEnchants.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				Enums.SwordEnchants enchants = SwordEnchants.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});

			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), SwordEnchants.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemBow) {
			ArrayList<Integer> collisions = BowEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<Enums.BowEnchant, Integer> opened = BowEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				Enums.BowEnchant enchants = BowEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), PickaxeEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemPickaxe) {
			ArrayList<Integer> collisions = PickaxeEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<PickaxeEnchant, Integer> opened = PickaxeEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				PickaxeEnchant enchants = PickaxeEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), PickaxeEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemSpade) {
			ArrayList<Integer> collisions = ShovelEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<ShovelEnchant, Integer> opened = ShovelEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				ShovelEnchant enchants = ShovelEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), ShovelEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemAxe) {
			ArrayList<Integer> collisions = AxeEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<AxeEnchant, Integer> opened = AxeEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				AxeEnchant enchants = AxeEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), AxeEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemHoe) {
			ArrayList<Integer> collisions = HoeEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<HoeEnchant, Integer> opened = HoeEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				HoeEnchant enchants = HoeEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), HoeEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemFlintAndSteel || item instanceof ItemFishFood || item instanceof ItemShield) {
			ArrayList<Integer> collisions = OtherEnchant.getCollisionsIdByLvl(curlvl);
			HashMap<OtherEnchant, Integer> opened = OtherEnchant.getOpened(curlvl);
			collisions.removeIf(integer -> !opened.keySet()
					.stream()
					.map(k -> k.id)
					.collect(Collectors.toList())
					.contains(integer));

			collisions.forEach(e -> {
				OtherEnchant enchants = OtherEnchant.getByOrdinal(e);
				map.put(enchants.id, enchants.name());
			});
			map.entrySet()
					.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), OtherEnchant.getByOrdinal(e
							.getKey()).enchantment));
		} else if (item instanceof ItemArmor) {
			ItemArmor cast = (ItemArmor) stack.getItem();
			EntityEquipmentSlot type = cast.armorType;
			if (type == EntityEquipmentSlot.HEAD) {
				ArrayList<Integer> collisions = HelmetEnchant.getCollisionsIdByLvl(curlvl);
				HashMap<HelmetEnchant, Integer> opened = HelmetEnchant.getOpened(curlvl);
				collisions.removeIf(integer -> !opened.keySet()
						.stream()
						.map(k -> k.id)
						.collect(Collectors.toList())
						.contains(integer));

				collisions.forEach(e -> {
					HelmetEnchant enchants = HelmetEnchant.getByOrdinal(e);
					map.put(enchants.id, enchants.name());
				});
				map.entrySet()
						.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), HelmetEnchant
								.getByOrdinal(e.getKey()).enchantment));
			} else if (type == EntityEquipmentSlot.LEGS || type == EntityEquipmentSlot.CHEST) {
				ArrayList<Integer> collisions = ChestplateLeggingsEnchant.getCollisionsIdByLvl(curlvl);
				HashMap<ChestplateLeggingsEnchant, Integer> opened = ChestplateLeggingsEnchant.getOpened(curlvl);
				collisions.removeIf(integer -> !opened.keySet()
						.stream()
						.map(k -> k.id)
						.collect(Collectors.toList())
						.contains(integer));

				collisions.forEach(e -> {
					ChestplateLeggingsEnchant enchants = ChestplateLeggingsEnchant.getByOrdinal(e);
					map.put(enchants.id, enchants.name());
				});
				map.entrySet()
						.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), ChestplateLeggingsEnchant
								.getByOrdinal(e.getKey()).enchantment));
			} else if (type == EntityEquipmentSlot.FEET) {
				ArrayList<Integer> collisions = BootsEnchant.getCollisionsIdByLvl(curlvl);
				HashMap<BootsEnchant, Integer> opened = BootsEnchant.getOpened(curlvl);
				collisions.removeIf(integer -> !opened.keySet()
						.stream()
						.map(k -> k.id)
						.collect(Collectors.toList())
						.contains(integer));

				collisions.forEach(e -> {
					BootsEnchant enchants = BootsEnchant.getByOrdinal(e);
					map.put(enchants.id, enchants.name());
				});
				map.entrySet()
						.removeIf(e -> ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), BootsEnchant.getByOrdinal(e
								.getKey()).enchantment));
			}
		}
		return map;
	}

	public enum Page {
		Reenchant,
		Rename,
		Repair
	}

	private class ScrollText extends GuiScrollingList {
		ItemStack stack;

		public ScrollText(Minecraft client, int width, int height, int top, int bottom, int left, int entryHeight, int screenWidth, int screenHeight, ItemStack stack) {
			super(client, width, height, top, bottom, left, entryHeight, screenWidth, screenHeight);
			this.stack = stack;
		}

		public void setStack(ItemStack stack) {
			this.stack = stack;
		}

		@Override
		protected int getSize() {
			return map.size();
		}

		@Override
		protected void elementClicked(int index, boolean doubleClick) {
			try {
				selectedId = getByIndex(index);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected boolean isSelected(int index) {
			return selectedId == getByIndex(index);
		}

		@Override
		protected void drawBackground() {

		}

		@Override
		protected void drawSlot(int slotIdx, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
			if (slotIdx % 2 == 0) drawRect(left, slotTop, left + width, slotTop + slotHeight, 0x1A000000);
			if (slotIdx == selectedIndex) drawRect(left, slotTop, left + width, slotTop + slotHeight, 0x44005555);
			int color = 0xFFFFFF;
			mc.fontRenderer.drawString(map.get(getByIndex(slotIdx)), left + 2, slotTop + 4, color);
		}
	}
}
