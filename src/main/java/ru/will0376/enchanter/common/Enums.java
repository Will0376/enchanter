package ru.will0376.enchanter.common;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Enchantments;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Enums {
	public enum SwordEnchants {
		Sharpness(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Collections.singletonList(5), Enchantments.SHARPNESS),
		FireAspect(1, 2, Arrays.asList(5, 10), Enchantments.FIRE_ASPECT),
		Looting(2, 3, Arrays.asList(4, 8, 12), Enchantments.LOOTING),
		Knockback(3, 3, Arrays.asList(3, 7, 11), Collections.singletonList(6), Enchantments.KNOCKBACK),
		Unbreaking(4, 4, Arrays.asList(1, 5, 8, 11), Enchantments.UNBREAKING),
		Smite(5, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Collections.singletonList(0), Enchantments.SMITE),
		SweepingEdge(6, 3, Arrays.asList(3, 6, 9), Collections.singletonList(3), Enchantments.SWEEPING);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		SwordEnchants(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			this.collisions = new ArrayList<>();
			Collections.reverse(this.open);
		}

		SwordEnchants(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(SwordEnchants ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<SwordEnchants, Integer> getOpened(int curlvl) {
			HashMap<SwordEnchants, Integer> ret = new HashMap<>();
			for (SwordEnchants enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<SwordEnchants> getByCurLvl(int curlvl) {
			ArrayList<SwordEnchants> enchants = new ArrayList<>();
			for (SwordEnchants enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static SwordEnchants getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum BowEnchant {
		Power(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Enchantments.POWER),
		Punch(1, 3, Arrays.asList(3, 8, 12), Enchantments.PUNCH),
		Flame(2, 3, Arrays.asList(3, 7, 12), Enchantments.FLAME),
		Infinity(3, 1, Collections.singletonList(12), Enchantments.INFINITY),
		Unbreaking(4, 4, Arrays.asList(1, 5, 8, 11), Enchantments.UNBREAKING);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public Enchantment enchantment;
		public List<Integer> collisions = new ArrayList<>();

		BowEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(BowEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<BowEnchant, Integer> getOpened(int curlvl) {
			HashMap<BowEnchant, Integer> ret = new HashMap<>();
			for (BowEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<BowEnchant> getByCurLvl(int curlvl) {
			ArrayList<BowEnchant> enchants = new ArrayList<>();
			for (BowEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static BowEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum ShovelEnchant {
		Efficiency(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Enchantments.EFFICIENCY),
		Unbreaking(1, 4, Arrays.asList(1, 6, 9, 12), Enchantments.UNBREAKING),
		Fortune(2, 4, Arrays.asList(3, 6, 9, 12), Collections.singletonList(3), Enchantments.FORTUNE),
		SilkTouch(3, 1, Collections.singletonList(5), Collections.singletonList(2), Enchantments.SILK_TOUCH);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		ShovelEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);

		}

		ShovelEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			collisions = new ArrayList<>();
			Collections.reverse(this.open);

		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(ShovelEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<ShovelEnchant, Integer> getOpened(int curlvl) {
			HashMap<ShovelEnchant, Integer> ret = new HashMap<>();
			for (ShovelEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<ShovelEnchant> getByCurLvl(int curlvl) {
			ArrayList<ShovelEnchant> enchants = new ArrayList<>();
			for (ShovelEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static ShovelEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum OtherEnchant {
		Unbreaking(0, 5, Arrays.asList(1, 5, 8, 11, 12), Enchantments.UNBREAKING);
		public int maxLvl;
		public List<Integer> open;
		public int id;
		public Enchantment enchantment;
		public List<Integer> collisions;

		OtherEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(OtherEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<OtherEnchant, Integer> getOpened(int curlvl) {
			HashMap<OtherEnchant, Integer> ret = new HashMap<>();
			for (OtherEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<OtherEnchant> getByCurLvl(int curlvl) {
			ArrayList<OtherEnchant> enchants = new ArrayList<>();
			for (OtherEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static OtherEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum HoeEnchant {
		Unbreaking(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Enchantments.UNBREAKING);
		public int maxLvl;
		public List<Integer> open;
		public int id;
		public Enchantment enchantment;
		public List<Integer> collisions;

		HoeEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static int getNextlvl(HoeEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static HashMap<HoeEnchant, Integer> getOpened(int curlvl) {
			HashMap<HoeEnchant, Integer> ret = new HashMap<>();
			for (HoeEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<HoeEnchant> getByCurLvl(int curlvl) {
			ArrayList<HoeEnchant> enchants = new ArrayList<>();
			for (HoeEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static HoeEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum AxeEnchant {
		Efficiency(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Enchantments.EFFICIENCY),
		Unbreaking(1, 4, Arrays.asList(1, 6, 9, 12), Enchantments.UNBREAKING),
		SilkTouch(2, 1, Arrays.asList(5), Enchantments.SILK_TOUCH),//?
		Knockback(3, 3, Arrays.asList(3, 7, 11), Enchantments.KNOCKBACK),//?
		Sharpness(4, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Arrays.asList(6), Enchantments.SHARPNESS),
		FireAspect(5, 1, Arrays.asList(12), Enchantments.FIRE_ASPECT), //?
		Smite(6, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Arrays.asList(4), Enchantments.SMITE);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		AxeEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.enchantment = enchantment;
			collisions = new ArrayList<>();
			Collections.reverse(this.open);
		}

		AxeEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(AxeEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<AxeEnchant, Integer> getOpened(int curlvl) {
			HashMap<AxeEnchant, Integer> ret = new HashMap<>();
			for (AxeEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<AxeEnchant> getByCurLvl(int curlvl) {
			ArrayList<AxeEnchant> enchants = new ArrayList<>();
			for (AxeEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static AxeEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum PickaxeEnchant {
		Efficiency(0, 6, Arrays.asList(2, 4, 6, 8, 10, 12), Enchantments.EFFICIENCY),
		Unbreaking(1, 4, Arrays.asList(1, 6, 9, 12), Enchantments.UNBREAKING),
		Fortune(2, 4, Arrays.asList(3, 6, 9, 12), Collections.singletonList(3), Enchantments.FORTUNE),
		SilkTouch(3, 1, Collections.singletonList(5), Collections.singletonList(2), Enchantments.SILK_TOUCH);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		PickaxeEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			collisions = new ArrayList<>();
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		PickaxeEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(PickaxeEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<PickaxeEnchant, Integer> getOpened(int curlvl) {
			HashMap<PickaxeEnchant, Integer> ret = new HashMap<>();
			for (PickaxeEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<PickaxeEnchant> getByCurLvl(int curlvl) {
			ArrayList<PickaxeEnchant> enchants = new ArrayList<>();
			for (PickaxeEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static PickaxeEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum BootsEnchant {
		Unbreaking(0, 4, Arrays.asList(1, 5, 8, 11), Enchantments.UNBREAKING),
		Thorns(1, 4, Arrays.asList(3, 6, 9, 12), Enchantments.THORNS),
		Protection(2, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(6, 7, 8), Enchantments.PROTECTION),
		FeatherFalling(3, 1, Collections.singletonList(12), Enchantments.FEATHER_FALLING),
		DepthStrider(4, 2, Arrays.asList(6, 12), Collections.singletonList(5), Enchantments.DEPTH_STRIDER),
		FrostWalker(5, 2, Arrays.asList(6, 12), Collections.singletonList(4), Enchantments.FROST_WALKER),
		ProjectileProtection(6, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 7, 8), Enchantments.PROJECTILE_PROTECTION),
		BlastProtection(7, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 6, 8), Enchantments.BLAST_PROTECTION),
		FireProtection(8, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 6, 7), Enchantments.FIRE_PROTECTION);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		BootsEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			collisions = new ArrayList<>();
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		BootsEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(BootsEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<BootsEnchant, Integer> getOpened(int curlvl) {
			HashMap<BootsEnchant, Integer> ret = new HashMap<>();
			for (BootsEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<BootsEnchant> getByCurLvl(int curlvl) {
			ArrayList<BootsEnchant> enchants = new ArrayList<>();
			for (BootsEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static BootsEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum HelmetEnchant {
		Unbreaking(0, 4, Arrays.asList(1, 5, 8, 11), Enchantments.UNBREAKING),
		Thorns(1, 4, Arrays.asList(3, 6, 9, 12), Enchantments.THORNS),
		Protection(2, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(5, 6, 7), Enchantments.PROTECTION),
		AquaAffinity(3, 1, Collections.singletonList(12), Enchantments.AQUA_AFFINITY),
		Respiration(4, 3, Arrays.asList(4, 8, 12), Enchantments.RESPIRATION),
		ProjectileProtection(5, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 6, 7), Enchantments.PROJECTILE_PROTECTION),
		BlastProtection(6, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 5, 7), Enchantments.BLAST_PROTECTION),
		FireProtection(7, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 5, 6), Enchantments.FIRE_PROTECTION);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		HelmetEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			collisions = new ArrayList<>();
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		HelmetEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(HelmetEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<HelmetEnchant, Integer> getOpened(int curlvl) {
			HashMap<HelmetEnchant, Integer> ret = new HashMap<>();
			for (HelmetEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<HelmetEnchant> getByCurLvl(int curlvl) {
			ArrayList<HelmetEnchant> enchants = new ArrayList<>();
			for (HelmetEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static HelmetEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum ChestplateLeggingsEnchant {
		Unbreaking(0, 4, Arrays.asList(1, 5, 8, 11), Enchantments.UNBREAKING),
		Thorns(1, 4, Arrays.asList(3, 6, 9, 12), Enchantments.THORNS),
		Protection(2, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(3, 4, 5), Enchantments.PROTECTION),
		ProjectileProtection(3, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 4, 5), Enchantments.PROJECTILE_PROTECTION),
		BlastProtection(4, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 3, 5), Enchantments.BLAST_PROTECTION),
		FireProtection(5, 5, Arrays.asList(2, 5, 7, 10, 12), Arrays.asList(2, 3, 4), Enchantments.FIRE_PROTECTION);
		public int id;
		public int maxLvl;
		public List<Integer> open;
		public List<Integer> collisions;
		public Enchantment enchantment;

		ChestplateLeggingsEnchant(int id, int maxLvl, List<Integer> open, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			collisions = new ArrayList<>();
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		ChestplateLeggingsEnchant(int id, int maxLvl, List<Integer> open, List<Integer> collisions, Enchantment enchantment) {
			this.id = id;
			this.maxLvl = maxLvl;
			this.open = open;
			this.collisions = collisions;
			this.enchantment = enchantment;
			Collections.reverse(this.open);
		}

		public static ArrayList<Integer> getCollisionsIdByLvl(int curlvl) {
			ArrayList<Integer> ret = new ArrayList<>();
			getOpened(curlvl).forEach((en, integer) -> {
				if (!en.collisions.isEmpty()) en.collisions.forEach(e -> {
					if (!ret.contains(e)) ret.add(e);
				});
			});
			return ret;
		}

		public static int getNextlvl(ChestplateLeggingsEnchant ench, int curlvl) {
			AtomicInteger ret = new AtomicInteger();
			getOpened(curlvl).forEach((key, value) -> {
				if (key == ench) ret.set(value + 1);
			});
			return ret.get();
		}

		public static HashMap<ChestplateLeggingsEnchant, Integer> getOpened(int curlvl) {
			HashMap<ChestplateLeggingsEnchant, Integer> ret = new HashMap<>();
			for (ChestplateLeggingsEnchant enc : values()) {
				for (Integer e : enc.open) {
					if (e <= curlvl && !ret.containsKey(enc)) {
						ret.put(enc, enc.open.size() - enc.open.indexOf(e));
					}
				}
			}
			return ret;
		}

		public static ArrayList<ChestplateLeggingsEnchant> getByCurLvl(int curlvl) {
			ArrayList<ChestplateLeggingsEnchant> enchants = new ArrayList<>();
			for (ChestplateLeggingsEnchant enc : values()) {
				if (enc.open.contains(curlvl)) enchants.add(enc);
			}
			return enchants;
		}

		public static ChestplateLeggingsEnchant getByOrdinal(int ordinal) {
			return values()[ordinal];
		}
	}

	public enum EnchantNeck {
		One(50, 100, 100, 8.3f, 75, 100, 0.8f, 7, 100),
		Tho(16.7f, 100, 100, 2.8f, 25f, 100, 0.3f, 2.3f, 100),
		Three(6.7f, 60f, 100, 1.1f, 10, 100, 0.1f, 1f, 60f),
		Four(2.6f, 23.7f, 100, 0.4f, 4, 100, 0.4f, 0.4f, 23.7f),
		Five(1.1f, 10, 100, 0.2f, 1.7f, 100, 0.01f, 0.156f, 10f),
		Six(0.4f, 3.8f, 100, 0.07f, 0.6f, 41, 0.007f, 0.06f, 3.8f),
		Seven(0.2f, 1.5f, 97.7f, 0.3f, 0.3f, 16.3f, 0.01f, 0.02f, 1.5f),
		Eight(0.07f, 0.6f, 39, 0.01f, 0.1f, 6.5f, 0.01f, 0.01f, 0.6f),
		Nine(0.03f, 0.24f, 15, 0.005f, 0.04f, 2.6f, 0.0004f, 0.003f, 0.244f),
		Ten(0.01f, 0.09f, 6.25f, 0.002f, 0.06f, 1.04f, 0.0002f, 0.002f, 0.1f),
		Eleven(0.004f, 0.04f, 2.5f, 0.0007f, 0.0065f, 0.4f, -1, -1, -1),
		Twelve(0.002f, 0.015f, 1, 0.0002f, 0.003f, 0.2f, -1, -1, -1),
		NULL(-1, -1, -1, -1, -1, -1, -1, -1, -1);
		public float dia1;
		public float dia2;
		public float dia3;
		public float gold1;
		public float gold2;
		public float gold3;
		public float iron1;
		public float iron2;
		public float iron3;

		EnchantNeck(float dia1, float dia2, float dia3, float gold1, float gold2, float gold3, float iron1, float iron2, float iron3) {
			this.dia1 = dia1;
			this.dia2 = dia2;
			this.dia3 = dia3;
			this.gold1 = gold1;
			this.gold2 = gold2;
			this.gold3 = gold3;
			this.iron1 = iron1;
			this.iron2 = iron2;
			this.iron3 = iron3;
		}

		public static EnchantNeck getById(int id) {
			if (id >= values().length) return NULL;
			return values()[id];
		}
	}
}
