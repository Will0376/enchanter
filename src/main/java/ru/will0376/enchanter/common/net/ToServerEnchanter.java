package ru.will0376.enchanter.common.net;

import io.netty.buffer.ByteBuf;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.Slot;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagIntArray;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.Enums.*;
import ru.will0376.enchanter.common.ModBlock;
import ru.will0376.enchanter.common.container.EnchanterContainer;

import java.util.*;
import java.util.stream.Collectors;

public class ToServerEnchanter implements IMessage {
	public static String nbtEnchantLvlConstant = "enchanterlvl";
	public static String nbtEnchantScrollConstant = "enchanterScroll";
	Action action;

	public ToServerEnchanter() {
	}

	public ToServerEnchanter(Action action) {
		this.action = action;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		Invoke.server(() -> action = Action.getById(buf.readInt()));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
	}

	public enum Action {
		Enchant;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToServerEnchanter, ToClientEnchanter> {

		public static String enchantConstant = "ENBTC";
		double random = 0;

		@GradleSideOnly(GradleSide.SERVER)
		public static void setEnchant(ItemStack stack, int curlvl) {
			Item item = stack.getItem();
			NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
			List<Integer> array = new ArrayList<>();
			List<Integer> collisionsId = new ArrayList<>();
			if (!nbt.isEmpty() && nbt.hasKey(enchantConstant))
				array = Arrays.stream(nbt.getIntArray(enchantConstant)).boxed().collect(Collectors.toList());

			if (item instanceof ItemSword) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					array.forEach(e -> collisionsId.addAll(SwordEnchants.getByOrdinal(e).collisions));
					ArrayList<SwordEnchants> inputlist = SwordEnchants.getByCurLvl(curlvl + 1);
					for (SwordEnchants e : inputlist) {
						if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
							int tmpint = e.id;
							collisionsId.addAll(e.collisions);
							if (array.contains(tmpint)) {
								int lvl = SwordEnchants.getNextlvl(e, curlvl);
								removeEnchant(stack, e.enchantment);
								stack.addEnchantment(e.enchantment, lvl);

							} else {
								array.add(e.id);
								nbt.setTag(enchantConstant, new NBTTagIntArray(array));
								stack.setTagCompound(nbt);
								stack.addEnchantment(e.enchantment, 1);

							}
						}
					}
				}
				if (curlvl == 0) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{SwordEnchants.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(SwordEnchants.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemBow) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					ArrayList<BowEnchant> inputlist = BowEnchant.getByCurLvl(curlvl + 1);
					for (BowEnchant e : inputlist) {
						int tmpint = e.id;
						if (array.contains(tmpint)) {
							int lvl = BowEnchant.getNextlvl(e, curlvl);
							removeEnchant(stack, e.enchantment);
							stack.addEnchantment(e.enchantment, lvl);

						} else {
							array.add(e.id);
							nbt.setTag(enchantConstant, new NBTTagIntArray(array));
							stack.setTagCompound(nbt);
							stack.addEnchantment(e.enchantment, 1);

						}
					}
				}
				if (curlvl == 0) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{BowEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(BowEnchant.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemPickaxe) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					array.forEach(e -> collisionsId.addAll(PickaxeEnchant.getByOrdinal(e).collisions));
					ArrayList<PickaxeEnchant> inputlist = PickaxeEnchant.getByCurLvl(curlvl + 1);
					for (PickaxeEnchant e : inputlist) {
						if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
							int tmpint = e.id;
							collisionsId.addAll(e.collisions);
							if (array.contains(tmpint)) {
								int lvl = PickaxeEnchant.getNextlvl(e, curlvl);
								removeEnchant(stack, e.enchantment);
								stack.addEnchantment(e.enchantment, lvl);

							} else {
								array.add(e.id);
								nbt.setTag(enchantConstant, new NBTTagIntArray(array));
								stack.setTagCompound(nbt);
								stack.addEnchantment(e.enchantment, 1);

							}
						}
					}
				}
				if (curlvl == 0) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{PickaxeEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(PickaxeEnchant.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemAxe) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					array.forEach(e -> collisionsId.addAll(AxeEnchant.getByOrdinal(e).collisions));
					ArrayList<AxeEnchant> inputlist = AxeEnchant.getByCurLvl(curlvl + 1);
					for (AxeEnchant e : inputlist) {
						if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
							int tmpint = e.id;
							collisionsId.addAll(e.collisions);
							if (array.contains(tmpint)) {
								int lvl = AxeEnchant.getNextlvl(e, curlvl);
								removeEnchant(stack, e.enchantment);
								stack.addEnchantment(e.enchantment, lvl);

							} else {
								array.add(e.id);
								nbt.setTag(enchantConstant, new NBTTagIntArray(array));
								stack.setTagCompound(nbt);
								stack.addEnchantment(e.enchantment, 1);

							}
						}
					}
				}
				if (curlvl == 0) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{AxeEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(AxeEnchant.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemSpade) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					array.forEach(e -> collisionsId.addAll(ShovelEnchant.getByOrdinal(e).collisions));
					ArrayList<ShovelEnchant> inputlist = ShovelEnchant.getByCurLvl(curlvl + 1);
					for (ShovelEnchant e : inputlist) {
						if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
							int tmpint = e.id;
							collisionsId.addAll(e.collisions);
							if (array.contains(tmpint)) {
								int lvl = ShovelEnchant.getNextlvl(e, curlvl);
								removeEnchant(stack, e.enchantment);
								stack.addEnchantment(e.enchantment, lvl);

							} else {
								array.add(e.id);
								nbt.setTag(enchantConstant, new NBTTagIntArray(array));
								stack.setTagCompound(nbt);
								stack.addEnchantment(e.enchantment, 1);

							}
						}
					}
				}
				if (curlvl == 0) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{ShovelEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(ShovelEnchant.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemHoe) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					ArrayList<HoeEnchant> inputlist = HoeEnchant.getByCurLvl(curlvl + 1);
					for (HoeEnchant e : inputlist) {
						int tmpint = e.id;
						if (array.contains(tmpint)) {
							int lvl = HoeEnchant.getNextlvl(e, curlvl);
							removeEnchant(stack, e.enchantment);
							stack.addEnchantment(e.enchantment, lvl);

						} else {
							array.add(e.id);
							nbt.setTag(enchantConstant, new NBTTagIntArray(array));
							stack.setTagCompound(nbt);
							stack.addEnchantment(e.enchantment, 1);

						}
					}
				}
				if (curlvl == 1) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{HoeEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(HoeEnchant.Unbreaking.enchantment, 1);

				}
			} else if (item instanceof ItemArmor) {
				ItemArmor cast = (ItemArmor) stack.getItem();
				EntityEquipmentSlot type = cast.armorType;
				if (type == EntityEquipmentSlot.HEAD) {
					if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
						array.forEach(e -> collisionsId.addAll(HelmetEnchant.getByOrdinal(e).collisions));
						ArrayList<HelmetEnchant> inputlist = HelmetEnchant.getByCurLvl(curlvl + 1);
						for (HelmetEnchant e : inputlist) {
							if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
								int tmpint = e.id;
								collisionsId.addAll(e.collisions);
								if (array.contains(tmpint)) {
									int lvl = HelmetEnchant.getNextlvl(e, curlvl);
									removeEnchant(stack, e.enchantment);
									stack.addEnchantment(e.enchantment, lvl);

								} else {
									array.add(e.id);
									nbt.setTag(enchantConstant, new NBTTagIntArray(array));
									stack.setTagCompound(nbt);
									stack.addEnchantment(e.enchantment, 1);

								}
							}
						}
					}
					if (curlvl == 0) {
						nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{HelmetEnchant.Unbreaking.id}));
						stack.setTagCompound(nbt);
						stack.addEnchantment(HelmetEnchant.Unbreaking.enchantment, 1);

					}
				} else if (type == EntityEquipmentSlot.CHEST || type == EntityEquipmentSlot.LEGS) {
					if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
						array.forEach(e -> collisionsId.addAll(ChestplateLeggingsEnchant.getByOrdinal(e).collisions));
						ArrayList<ChestplateLeggingsEnchant> inputlist = ChestplateLeggingsEnchant.getByCurLvl(curlvl + 1);
						for (ChestplateLeggingsEnchant e : inputlist) {
							if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
								int tmpint = e.id;
								collisionsId.addAll(e.collisions);
								if (array.contains(tmpint)) {
									int lvl = ChestplateLeggingsEnchant.getNextlvl(e, curlvl);
									removeEnchant(stack, e.enchantment);
									stack.addEnchantment(e.enchantment, lvl);

								} else {
									array.add(e.id);
									nbt.setTag(enchantConstant, new NBTTagIntArray(array));
									stack.setTagCompound(nbt);
									stack.addEnchantment(e.enchantment, 1);

								}
							}
						}
					}
					if (curlvl == 0) {
						nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{
								ChestplateLeggingsEnchant.Unbreaking.id}));
						stack.setTagCompound(nbt);
						stack.addEnchantment(ChestplateLeggingsEnchant.Unbreaking.enchantment, 1);

					}
				} else if (type == EntityEquipmentSlot.FEET) {
					if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
						array.forEach(e -> collisionsId.addAll(BootsEnchant.getByOrdinal(e).collisions));
						ArrayList<BootsEnchant> inputlist = BootsEnchant.getByCurLvl(curlvl + 1);
						for (BootsEnchant e : inputlist) {
							if (e.collisions.isEmpty() || !collisionsId.contains(e.id)) {
								int tmpint = e.id;
								collisionsId.addAll(e.collisions);
								if (array.contains(tmpint)) {
									int lvl = BootsEnchant.getNextlvl(e, curlvl);
									removeEnchant(stack, e.enchantment);
									stack.addEnchantment(e.enchantment, lvl);

								} else {
									array.add(e.id);
									nbt.setTag(enchantConstant, new NBTTagIntArray(array));
									stack.setTagCompound(nbt);
									stack.addEnchantment(e.enchantment, 1);

								}
							}
						}
					}
					if (curlvl == 0) {
						nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{BootsEnchant.Unbreaking.id}));
						stack.setTagCompound(nbt);
						stack.addEnchantment(BootsEnchant.Unbreaking.enchantment, 1);

					}
				}
			} else if (item instanceof ItemFlintAndSteel || item instanceof ItemFishFood || item instanceof ItemShield) {
				if (!nbt.isEmpty() && nbt.hasKey(enchantConstant)) {
					ArrayList<OtherEnchant> inputlist = OtherEnchant.getByCurLvl(curlvl + 1);
					for (OtherEnchant e : inputlist) {
						int tmpint = e.id;
						if (array.contains(tmpint)) {
							int lvl = OtherEnchant.getNextlvl(e, curlvl);
							removeEnchant(stack, e.enchantment);
							stack.addEnchantment(e.enchantment, lvl);

						} else {
							array.add(e.id);
							nbt.setTag(enchantConstant, new NBTTagIntArray(array));
							stack.setTagCompound(nbt);
							stack.addEnchantment(e.enchantment, 1);

						}
					}
				}
				if (curlvl == 1) {
					nbt.setTag(enchantConstant, new NBTTagIntArray(new int[]{OtherEnchant.Unbreaking.id}));
					stack.setTagCompound(nbt);
					stack.addEnchantment(OtherEnchant.Unbreaking.enchantment, 1);

				}
			}
		}

		@GradleSideOnly(GradleSide.SERVER)
		public static void removeEnchant(ItemStack stack, Enchantment e) {
			NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
			if (Objects.requireNonNull(nbt).hasKey("ench") && containsInTag(nbt.getTagList("ench", 10), e)) {
				Iterator<NBTBase> iterator = nbt.getTagList("ench", 10).iterator();
				while (iterator.hasNext()) {
					NBTBase nbtBase = iterator.next();
					NBTTagCompound nbttg = (NBTTagCompound) nbtBase;
					if (nbttg.getShort("id") == Enchantment.getEnchantmentID(e)) {
						iterator.remove();

					}
				}
			}
		}

		public static float setChance(ItemStack inputTool, ItemStack inputEnchant, int curlvl, boolean enableRemove) {
			EnchantNeck enumm = EnchantNeck.getById(curlvl);
			float chance = 0;
			if (curlvl + 1 == 2 && inputEnchant.getItem() == Items.DIAMOND && inputEnchant.getCount() == 6) {
				chance = 100;
				if (enableRemove) setCount(inputEnchant, 6);
			} else if (inputEnchant.getItem() == Items.DIAMOND) {
				chance = enumm.dia1 * inputEnchant.getCount();
				if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
			} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.DIAMOND_BLOCK) && inputEnchant.getCount() == 64) {
				chance = enumm.dia3;
				if (enableRemove) setCount(inputEnchant, 64);
			} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.DIAMOND_BLOCK)) {
				chance = enumm.dia2 * inputEnchant.getCount();
				if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
			} else if (inputEnchant.getItem() == Items.GOLD_INGOT) {
				chance = enumm.gold1 * inputEnchant.getCount();
				if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
			} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.GOLD_BLOCK) && inputEnchant.getCount() == 64) {
				chance = enumm.gold3 * inputEnchant.getCount();
				if (enableRemove) setCount(inputEnchant, 64);
			} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.GOLD_BLOCK)) {
				chance = enumm.gold2 * inputEnchant.getCount();
				if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
			} else if (enumm.iron1 != -1 && (inputEnchant.getItem() == Items.IRON_INGOT || inputEnchant.getItem() == Item
					.getItemFromBlock(Blocks.IRON_BLOCK))) {
				if (inputEnchant.getItem() == Items.IRON_INGOT) {
					chance = enumm.iron1 * inputEnchant.getCount();
					if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
				} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.IRON_BLOCK) && inputEnchant.getCount() == 64) {
					chance = enumm.iron3 * inputEnchant.getCount();
					if (enableRemove) setCount(inputEnchant, 64);
				} else if (inputEnchant.getItem() == Item.getItemFromBlock(Blocks.IRON_BLOCK)) {
					chance = enumm.iron2 * inputEnchant.getCount();
					if (enableRemove) setCount(inputEnchant, inputEnchant.getCount());
				}
			} else if (inputEnchant.getItem() == ModBlock.ball) {
				int ballLvl = inputEnchant.getMetadata();
				if (curlvl == ballLvl) {
					chance = 100;
					if (enableRemove) setCount(inputEnchant, 1);
				} else if (enableRemove) return -999;
			} else if (inputEnchant.getItem() == ModBlock.scroll) {
				chance += 10 + (inputEnchant.getMetadata() * 5);
			} else if (enableRemove) return -999;
			if (inputTool.hasTagCompound() && inputTool.getTagCompound().hasKey(nbtEnchantScrollConstant)) {
				int scrollLvl = inputTool.getTagCompound().getInteger(nbtEnchantScrollConstant);
				chance += 10 + (scrollLvl * 5);
				if (enableRemove) inputTool.getTagCompound().removeTag(nbtEnchantScrollConstant);
			}
			return chance;
		}

		private static void setCount(ItemStack stack, int minus) {
			stack.setCount(stack.getCount() - minus);
		}

		public static boolean containsInTag(NBTTagList tagList, Enchantment enchantment) {
			for (NBTBase e : tagList) {
				NBTTagCompound nbt = (NBTTagCompound) e;
				if (nbt.getShort("id") == Enchantment.getEnchantmentID(enchantment)) return true;
			}
			return false;
		}

		@Override
		public ToClientEnchanter onMessage(ToServerEnchanter message, MessageContext ctx) {
			return Invoke.serverValue(() -> onMessager(message, ctx));
		}

		@GradleSideOnly(GradleSide.SERVER)
		public ToClientEnchanter onMessager(ToServerEnchanter message, MessageContext ctx) {
			try {
				if (ctx.getServerHandler().player.openContainer instanceof EnchanterContainer && message.action == Action.Enchant) {
					EnchanterContainer container = (EnchanterContainer) ctx.getServerHandler().player.openContainer;
					Slot inputToolSlot = container.getSlot(0);
					Slot inputEnchantSlot = container.getSlot(1);
					Slot lapisSlot = container.getSlot(2);
					Slot outputSlot = container.getSlot(3);
					if (!lapisSlot.getHasStack())
						return new ToClientEnchanter(ToClientEnchanter.Action.ErrorMessage, new TextComponentTranslation("process.lapis.error")
								.getFormattedText());

					if (inputToolSlot.getHasStack() && inputEnchantSlot.getHasStack() && lapisSlot.getHasStack() && !outputSlot
							.getHasStack()) {
						ItemStack inputTool = inputToolSlot.getStack();
						ItemStack inputEnchant = inputEnchantSlot.getStack();
						ItemStack lapis = lapisSlot.getStack();

						if (inputEnchant.getItem() == ModBlock.scroll) {
							if (inputTool.hasTagCompound() && inputTool.getTagCompound()
									.hasKey(nbtEnchantScrollConstant) && inputTool.getTagCompound()
									.getInteger(nbtEnchantScrollConstant) >= inputEnchant.getMetadata())
								return new ToClientEnchanter(ToClientEnchanter.Action.ErrorMessage, new TextComponentTranslation("process.scroll.lvlerror")
										.getFormattedText());
							else {
								ItemStack out = inputTool.copy();
								NBTTagCompound nbt = inputTool.hasTagCompound() ? inputTool.getTagCompound() : new NBTTagCompound();
								nbt.setInteger(nbtEnchantScrollConstant, inputEnchant.getMetadata());
								out.setTagCompound(nbt);
								setCount(inputEnchant, 1);
								inputToolSlot.putStack(new ItemStack(Items.AIR));
								outputSlot.putStack(out);
								return new ToClientEnchanter(ToClientEnchanter.Action.Success, new TextComponentTranslation("process.scroll.succ")
										.getFormattedText());
							}
						}
						boolean firstEnchant = true;
						if (inputTool.hasTagCompound() && inputTool.getTagCompound().hasKey(nbtEnchantLvlConstant))
							firstEnchant = false;

						int curlvl = firstEnchant ? 0 : inputTool.getTagCompound().getInteger(nbtEnchantLvlConstant);
						if (lapis.getCount() < 3 * (curlvl + 1))
							return new ToClientEnchanter(ToClientEnchanter.Action.ErrorMessage, new TextComponentTranslation("process.lapis.error")
									.getFormattedText());

						float chance = setChance(inputTool, inputEnchant, curlvl, true);

						if (chance == -999 || chance == -1)
							return new ToClientEnchanter(ToClientEnchanter.Action.ErrorMessage, new TextComponentTranslation("process.incorrect.item.error")
									.getFormattedText());
						if (!Enchanter.debug) lapis.setCount(lapis.getCount() - (3 * (curlvl + 1)));
						if (randomize(chance)) {
							IThreadListener listener = ctx.getServerHandler().player.getServerWorld();
							listener.addScheduledTask(() -> {
								setEnchant(inputTool, curlvl);

								ItemStack out = inputTool.copy();
								NBTTagCompound nbt = (out.getTagCompound() == null ? new NBTTagCompound() : out.getTagCompound());
								nbt.setInteger(nbtEnchantLvlConstant, curlvl + 1);
								out.setTagCompound(nbt);
								out.setStackDisplayName(out.getDisplayName()
										.replace(" +" + curlvl, "") + " +" + (curlvl + 1));
								outputSlot.putStack(out);
								inputToolSlot.putStack(new ItemStack(Items.AIR));
								ctx.getServerHandler().player.world.playSound(null, ctx.getServerHandler().player.getPosition(), SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 1.0F, ctx
										.getServerHandler().player.world.rand.nextFloat() * 0.1F + 0.9F);
							});
							return new ToClientEnchanter(ToClientEnchanter.Action.Success, new TextComponentTranslation("process.success")
									.getFormattedText(), (float) random, chance);
						} else
							return new ToClientEnchanter(ToClientEnchanter.Action.Fail, new TextComponentTranslation("process.fail")
									.getFormattedText(), (float) random, chance);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		@GradleSideOnly(GradleSide.SERVER)
		private boolean randomize(float input) {
			input /= 100;
			random = new Random().nextDouble();
			if (Enchanter.debug) System.out.println("random = " + random);
			if (Enchanter.debug) System.out.println("chance = " + input);
			return random < input;
		}
	}
}
