package org.spongepowered.api.scheduler;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

@GradleSideOnly(GradleSide.SERVER)
public interface Task {
	static Task.Builder builder() {
		return new Builder() {
		};
	}

	interface Builder {
		default Task.Builder execute(Runnable runnable) {
			runnable.run();
			return null;
		}
	}
}
