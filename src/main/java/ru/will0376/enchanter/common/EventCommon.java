package ru.will0376.enchanter.common;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.blocks.Discoverer;
import ru.will0376.enchanter.common.blocks.ReplacedAnvil;
import ru.will0376.enchanter.common.blocks.ReplacedEnchanter;
import ru.will0376.enchanter.common.item.Ball;
import ru.will0376.enchanter.common.item.Scroll;
import ru.will0376.enchanter.common.tileentity.TEAnvil;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

@Mod.EventBusSubscriber
public class EventCommon {
	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().register(new ReplacedEnchanter());
		event.getRegistry().register(new Discoverer());
		event.getRegistry().register(new ReplacedAnvil());

		GameRegistry.registerTileEntity(TEEnchanter.class, new ResourceLocation(Enchanter.MOD_ID, "_replacedenchanter"));
		GameRegistry.registerTileEntity(TEAnvil.class, new ResourceLocation(Enchanter.MOD_ID, "_replacedanvil"));
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().register(new Ball());
		event.getRegistry().register(new Scroll());
		event.getRegistry()
				.register(new ItemBlock(ModBlock.replacedenchanter).setRegistryName(ModBlock.replacedenchanter.getRegistryName()));
		event.getRegistry()
				.register(new ItemBlock(ModBlock.replacedAnvil).setRegistryName(ModBlock.replacedAnvil.getRegistryName()));
		event.getRegistry()
				.register(new ItemBlock(ModBlock.discoverer).setRegistryName(ModBlock.discoverer.getRegistryName()));
	}

	@SubscribeEvent
	public static void registerRecipes(RegistryEvent.Register<IRecipe> event) {
		try {
			ResourceLocation theButton = new ResourceLocation("enchanting_table");
			ShapedRecipes shapedRecipes = (ShapedRecipes) event.getRegistry().getValue(theButton);

			setPublicFinalField(ObfuscationReflectionHelper.findField(shapedRecipes.getClass(), "field_77575_e"), shapedRecipes, new ItemStack(ModBlock.replacedenchanter));

			theButton = new ResourceLocation("anvil");
			shapedRecipes = (ShapedRecipes) event.getRegistry().getValue(theButton);
			setPublicFinalField(ObfuscationReflectionHelper.findField(shapedRecipes.getClass(), "field_77575_e"), shapedRecipes, new ItemStack(ModBlock.replacedAnvil));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void setPublicFinalField(Field field, Object classinst, Object object) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(classinst, object);
	}
}
