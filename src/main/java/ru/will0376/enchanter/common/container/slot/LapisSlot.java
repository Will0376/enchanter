package ru.will0376.enchanter.common.container.slot;

import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class LapisSlot extends SlotItemHandler {

	public LapisSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return stack.getItem() == Items.DYE && stack.getMetadata() == EnumDyeColor.BLUE.getDyeDamage();
	}
}
