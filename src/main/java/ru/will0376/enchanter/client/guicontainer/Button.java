package ru.will0376.enchanter.client.guicontainer;

import net.minecraft.client.gui.GuiButton;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class Button extends GuiButton {
	public Button(int buttonId, int x, int y, String buttonText) {
		super(buttonId, x, y, buttonText);
	}

	public Button(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
	}

	public Button setText(String text) {
		this.displayString = text;
		return this;
	}

	public Button invertEnableButton() {
		this.enabled = !this.enabled;
		return this;
	}

	public Button setEnableButton(boolean bool) {
		this.enabled = bool;
		return this;
	}

	public Button disableText() {
		displayString = "";
		return this;
	}
}
