package ru.will0376.enchanter.client.guicontainer;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.client.GuiHelper;
import ru.will0376.enchanter.common.ModBlock;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToServerDiscoverer;
import ru.will0376.enchanter.server.PlayerPojo;

import java.awt.*;
import java.io.IOException;
import java.util.Objects;

public class DiscovererGui extends GuiScreen {
	private static final ResourceLocation background = new ResourceLocation(Enchanter.MOD_ID, "textures/gui/discoverer.png");
	public String lastText = "";
	public int tickText = 0;
	public Color color = Color.BLACK;
	int guiLeft, guiTop;
	int xSize = 176;
	int ySize = 80;
	GuiButton button;
	int reqlvl = -1;
	ItemStack stack1 = ItemStack.EMPTY, stack2 = ItemStack.EMPTY;

	@Override
	public void initGui() {
		super.initGui();
		guiLeft = (this.width - this.xSize) / 2;
		guiTop = (this.height - this.ySize) / 2;
		button = addButton(new GuiButton(0, width / 2 - 50, height / 2, 100, 20, I18n.format("discoverer.gui.open")));
		PlayerPojo pojo = PlayerPojo.clientpojo;
		if (stack1 == ItemStack.EMPTY || stack2 == ItemStack.EMPTY) {
			if (pojo.getOpenedlvl() >= 6) {
				button.enabled = false;
				button.displayString = I18n.format("discoverer.gui.open");
			} else {
				Item item = ModBlock.ball;
				stack1 = new ItemStack(item, 1, (pojo.getOpenedlvl()) * 2);
				stack2 = new ItemStack(item, 1, (pojo.getOpenedlvl()) * 2 + 1);
			}
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			Net.get().sendToServer(new ToServerDiscoverer(ToServerDiscoverer.Action.OpenNext));
		}
		super.actionPerformed(button);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawDefaultBackground();
		mc.getTextureManager().bindTexture(background);
		drawTexturedModalRect(guiLeft + 1, guiTop - 14, 0, 0, xSize, ySize + 14);
		String header = I18n.format("discoverer.gui.opener.text");
		fontRenderer.drawString(header, width / 2 - fontRenderer.getStringWidth(header) / 2, height / 2 - 44, Color.WHITE
				.getRGB(), true);
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (tickText > 0)
			fontRenderer.drawString(lastText, width / 2 - this.fontRenderer.getStringWidth(lastText) / 2, height / 2 - 12, color
					.getRGB(), false);

		PlayerPojo pojo = PlayerPojo.clientpojo;
		switch (Objects.requireNonNull(pojo).getOpenedlvl()) {
			case 0:
				reqlvl = 30;
				break;
			case 1:
				reqlvl = 40;
				break;
			case 2:
				reqlvl = 60;
				break;
			case 3:
				reqlvl = 81;
				break;
			case 4:
				reqlvl = 150;
				break;
			case 5:
				reqlvl = 205;
				break;
		}
		if (reqlvl != -1) {

			GuiHelper.renderBlocks(width / 2 - 20, height / 2 - 70 + 38, stack1);
			GuiHelper.renderBlocks(width / 2, height / 2 - 70 + 38, stack2);

			if (tickText == 0) {
				String req = I18n.format("discoverer.gui.opener.req", reqlvl);
				fontRenderer.drawString(req, width / 2 - this.fontRenderer.getStringWidth(req) / 2, height / 2 - 52 + 38, mc.player.experienceLevel >= reqlvl ? Color.YELLOW
						.getRGB() : Color.RED.getRGB(), mc.player.experienceLevel >= reqlvl);
			}
			String text = I18n.format(stack1.getItem()
					.getTranslationKey()
					.replace("item.", "") + "." + stack1.getMetadata());
			int x = width / 2 - 20;
			int y = height / 2 - 70 + 38;
			if (mouseX >= x && mouseY >= y && mouseX < x + 16 && mouseY < y + 16)
				drawHoveringText(text, mouseX, mouseY);

			text = I18n.format(stack1.getItem().getTranslationKey().replace("item.", "") + "." + stack2.getMetadata());
			x = width / 2;
			y = height / 2 - 70 + 38;
			if (mouseX >= x && mouseY >= y && mouseX < x + 16 && mouseY < y + 16)
				drawHoveringText(text, mouseX, mouseY);
		}
	}

	@Override
	public void updateScreen() {
		super.updateScreen();
		if (tickText > 0) tickText--;
	}
}
