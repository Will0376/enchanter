package ru.will0376.enchanter.common.net;

import austeretony.oxygen_core.common.api.CommonReference;
import austeretony.oxygen_core.server.api.CurrencyHelperServer;
import io.netty.buffer.ByteBuf;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.Enums.*;
import ru.will0376.enchanter.common.container.AnvilContainer;

import static ru.will0376.enchanter.common.net.ToServerEnchanter.nbtEnchantLvlConstant;

public class ToServerAnvil implements IMessage {
	Action action;
	String text;

	public ToServerAnvil() {
	}

	public ToServerAnvil(Action action) {
		this.action = action;
		text = "";
	}

	public ToServerAnvil(Action action, String text) {
		this.action = action;
		this.text = text;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		Invoke.server(() -> {
			action = Action.getById(buf.readInt());
			text = ByteBufUtils.readUTF8String(buf);
		});
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
		ByteBufUtils.writeUTF8String(buf, text);
	}

	public enum Action {
		ReEnchant,
		Rename,
		Repair;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToServerAnvil, ToClientAnvil> {
		@Override
		public ToClientAnvil onMessage(ToServerAnvil toServerAnvil, MessageContext messageContext) {
			return Invoke.serverValue(() -> onMessager(toServerAnvil, messageContext));
		}

		@GradleSideOnly(GradleSide.SERVER)
		public ToClientAnvil onMessager(ToServerAnvil message, MessageContext ctx) {
			try {
				if (ctx.getServerHandler().player.openContainer instanceof AnvilContainer) {
					IThreadListener listener = ctx.getServerHandler().player.getServerWorld();
					listener.addScheduledTask(() -> {
						AnvilContainer container = (AnvilContainer) ctx.getServerHandler().player.openContainer;
						ItemStack input = container.getSlot(0).getStack();
						Item item = input.getItem();
						if (container.getSlot(0).getHasStack() && !container.getSlot(1).getHasStack()) {
							NBTTagCompound nbt = input.hasTagCompound() ? input.getTagCompound() : new NBTTagCompound();
							boolean firstEnchant = true;
							if (input.hasTagCompound() && input.getTagCompound().hasKey(nbtEnchantLvlConstant))
								firstEnchant = false;
							long balance = CurrencyHelperServer.getCurrency(CommonReference.getPersistentUUID(ctx.getServerHandler().player), Enchanter.config
									.getServerConfig()
									.getOxyCoinProviderIndex());
							int curlvl = firstEnchant ? 0 : input.getTagCompound().getInteger(nbtEnchantLvlConstant);
							switch (message.action) {
								case Rename:
									if (message.text.length() * Enchanter.config.getServerConfig()
											.getMultiple() / 2 <= balance) {
										CurrencyHelperServer.setCurrency(CommonReference.getPersistentUUID(ctx.getServerHandler().player), (long) (balance - message.text
												.length() * Enchanter.config.getServerConfig()
												.getMultiple() / 2), Enchanter.config.getServerConfig()
												.getOxyCoinProviderIndex());
										input.setStackDisplayName(message.text);
										container.getSlot(1).putStack(input);
										container.getSlot(0).putStack(ItemStack.EMPTY);
									} else {
										return;
									}

									break;
								case ReEnchant:
									if (message.text.isEmpty() || message.text.equals("-1")) return;
									if (item instanceof ItemSword) {
										SwordEnchants ench = SwordEnchants.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											SwordEnchants enchin = SwordEnchants.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, SwordEnchants.getOpened(curlvl)
												.get(SwordEnchants.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemBow) {
										BowEnchant ench = BowEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											BowEnchant enchin = BowEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, BowEnchant.getOpened(curlvl)
												.get(BowEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemAxe) {
										AxeEnchant ench = AxeEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											AxeEnchant enchin = AxeEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, AxeEnchant.getOpened(curlvl)
												.get(AxeEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemPickaxe) {
										PickaxeEnchant ench = PickaxeEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											PickaxeEnchant enchin = PickaxeEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, PickaxeEnchant.getOpened(curlvl)
												.get(PickaxeEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemSpade) {
										ShovelEnchant ench = ShovelEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											ShovelEnchant enchin = ShovelEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, ShovelEnchant.getOpened(curlvl)
												.get(ShovelEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemHoe) {
										HoeEnchant ench = HoeEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											HoeEnchant enchin = HoeEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, HoeEnchant.getOpened(curlvl)
												.get(HoeEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemFlintAndSteel || item instanceof ItemFishFood || item instanceof ItemShield) {
										OtherEnchant ench = OtherEnchant.getByOrdinal(Integer.parseInt(message.text));
										for (Integer e : ench.collisions) {
											OtherEnchant enchin = OtherEnchant.getByOrdinal(e);
											if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
												ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
											}
										}
										input.addEnchantment(ench.enchantment, OtherEnchant.getOpened(curlvl)
												.get(OtherEnchant.getByOrdinal(Integer.parseInt(message.text))));
									} else if (item instanceof ItemArmor) {
										ItemArmor cast = (ItemArmor) input.getItem();
										EntityEquipmentSlot type = cast.armorType;
										if (type == EntityEquipmentSlot.HEAD) {
											HelmetEnchant ench = HelmetEnchant.getByOrdinal(Integer.parseInt(message.text));
											for (Integer e : ench.collisions) {
												HelmetEnchant enchin = HelmetEnchant.getByOrdinal(e);
												if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
													ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
												}
											}
											input.addEnchantment(ench.enchantment, HelmetEnchant.getOpened(curlvl)
													.get(HelmetEnchant.getByOrdinal(Integer.parseInt(message.text))));
										} else if (type == EntityEquipmentSlot.CHEST || type == EntityEquipmentSlot.LEGS) {
											ChestplateLeggingsEnchant ench = ChestplateLeggingsEnchant.getByOrdinal(Integer
													.parseInt(message.text));
											for (Integer e : ench.collisions) {
												ChestplateLeggingsEnchant enchin = ChestplateLeggingsEnchant.getByOrdinal(e);
												if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
													ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
												}
											}
											input.addEnchantment(ench.enchantment, ChestplateLeggingsEnchant.getOpened(curlvl)
													.get(ChestplateLeggingsEnchant.getByOrdinal(Integer.parseInt(message.text))));
										} else if (type == EntityEquipmentSlot.FEET) {
											BootsEnchant ench = BootsEnchant.getByOrdinal(Integer.parseInt(message.text));
											for (Integer e : ench.collisions) {
												BootsEnchant enchin = BootsEnchant.getByOrdinal(e);
												if (ToServerEnchanter.Handler.containsInTag(nbt.getTagList("ench", 10), enchin.enchantment)) {
													ToServerEnchanter.Handler.removeEnchant(input, enchin.enchantment);
												}
											}
											input.addEnchantment(ench.enchantment, BootsEnchant.getOpened(curlvl)
													.get(BootsEnchant.getByOrdinal(Integer.parseInt(message.text))));
										}
									}
									break;
								case Repair:
									if (input.getItemDamage() * Enchanter.config.getServerConfig()
											.getMultiple() <= balance) {
										input.setItemDamage(0);
										CurrencyHelperServer.setCurrency(CommonReference.getPersistentUUID(ctx.getServerHandler().player), (long) (balance - input
												.getItemDamage() * Enchanter.config.getServerConfig()
												.getMultiple()), Enchanter.config.getServerConfig()
												.getOxyCoinProviderIndex());
									} else {
										return;
									}
									break;
							}
							container.getSlot(1).putStack(input);
							container.getSlot(0).putStack(ItemStack.EMPTY);
						}
					});
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.println("message.text = " + message.text);
			}
			return null;
		}
	}
}
