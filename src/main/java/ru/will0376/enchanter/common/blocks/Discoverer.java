package ru.will0376.enchanter.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.client.guicontainer.DiscovererGui;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToClientDiscoverer;

public class Discoverer extends Block {
	public Discoverer() {
		super(Material.PORTAL);
		setTranslationKey(Enchanter.MOD_ID + ".discoverer");
		setRegistryName("discoverer");
		setCreativeTab(Enchanter.tab);
	}

	@SideOnly(Side.CLIENT)
	public void initModel() {
		Minecraft.getMinecraft()
				.getRenderItem()
				.getItemModelMesher()
				.register(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		try {
			if (playerIn instanceof EntityPlayerSP) {
				if (FMLCommonHandler.instance().getSide().isClient()) openGui();
			} else {
				Net.get().sendTo(new ToClientDiscoverer(ToClientDiscoverer.Action.OpenGui), (EntityPlayerMP) playerIn);
			}
		} catch (NoClassDefFoundError ex) {
			Net.get().sendTo(new ToClientDiscoverer(ToClientDiscoverer.Action.OpenGui), (EntityPlayerMP) playerIn);
		}
		return true;
	}

	@SideOnly(Side.CLIENT)
	private void openGui() {
		Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().displayGuiScreen(new DiscovererGui()));
	}
}
