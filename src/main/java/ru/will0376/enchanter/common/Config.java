package ru.will0376.enchanter.common;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.client.ConfigClient;
import ru.will0376.enchanter.server.ConfigServer;

import java.io.File;

public abstract class Config {
	public final String GENERAL = "General";
	private final Configuration configuration;

	public Config(File configFile) {
		configuration = new Configuration(configFile);
	}

	public static Config getNewConfig(File file) {
		return FMLCommonHandler.instance()
				.getSide()
				.isClient() && !Enchanter.debug ? new ConfigClient(file) : Invoke.serverValue(() -> new ConfigServer(file));
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void save() {
		configuration.save();
	}

	public void load() {
		configuration.load();
	}

	public Config launch() {
		load();
		setConfigs();
		save();
		return this;
	}

	public abstract void setConfigs();

	public ConfigClient getClientConfig() {
		return (ConfigClient) this;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public ConfigServer getServerConfig() {
		return (ConfigServer) this;
	}

	public Config getConfigBySide() {
		return FMLCommonHandler.instance()
				.getSide()
				.isClient() && !Enchanter.debug ? getClientConfig() : Invoke.serverValue(this::getServerConfig);
	}
}
