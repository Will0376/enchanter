package ru.will0376.enchanter.common.net;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.server.PlayerPojo;

import java.util.Objects;

public class ToServerDiscoverer implements IMessage {
	Action action;
	String text;

	public ToServerDiscoverer() {
	}

	public ToServerDiscoverer(Action action) {
		this.action = action;
		text = "";
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		Invoke.server(() -> {
			action = Action.getById(buf.readInt());
			text = ByteBufUtils.readUTF8String(buf);
		});
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
		ByteBufUtils.writeUTF8String(buf, text);
	}

	public enum Action {
		OpenNext;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToServerDiscoverer, ToClientDiscoverer> {

		@GradleSideOnly(GradleSide.SERVER)
		public ToClientDiscoverer onMessager(ToServerDiscoverer message, MessageContext ctx) {
			if (message.action == Action.OpenNext) {

				EntityPlayerMP player = ctx.getServerHandler().player;
				int lvlnow = player.experienceLevel;
				int reqlvl = 0;
				PlayerPojo pojo = PlayerPojo.loadPlayerCfg(player.getName());
				switch (Objects.requireNonNull(pojo).getOpenedlvl()) {
					case 0:
						reqlvl = 30;
						break;
					case 1:
						reqlvl = 40;
						break;
					case 2:
						reqlvl = 60;
						break;
					case 3:
						reqlvl = 81;
						break;
					case 4:
						reqlvl = 150;
						break;
					case 5:
						reqlvl = 205;
						break;
				}
				if (lvlnow < reqlvl)
					return new ToClientDiscoverer(ToClientDiscoverer.Action.ErrorText, new TextComponentTranslation("process.fail")
							.getFormattedText());
				int finalReqlvl = reqlvl;

				IThreadListener listener = ctx.getServerHandler().player.getServerWorld();
				listener.addScheduledTask(() -> {
					player.addExperienceLevel(-finalReqlvl);
				});
				pojo.setOpenedlvl(pojo.getOpenedlvl() + 1).replaceInMap().saveCfg().sendToPlayer();
				return new ToClientDiscoverer(ToClientDiscoverer.Action.Success, new TextComponentTranslation("process.success")
						.getFormattedText());
			}
			return null;
		}

		@Override
		public ToClientDiscoverer onMessage(ToServerDiscoverer toServerDiscoverer, MessageContext messageContext) {
			return Invoke.serverValue(() -> onMessager(toServerDiscoverer, messageContext));
		}
	}
}
