package ru.will0376.enchanter.common;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.client.guicontainer.AnvilGuiContainer;
import ru.will0376.enchanter.client.guicontainer.EnchanterGuiContainer;
import ru.will0376.enchanter.common.container.AnvilContainer;
import ru.will0376.enchanter.common.container.EnchanterContainer;
import ru.will0376.enchanter.common.tileentity.TEAnvil;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

import javax.annotation.Nullable;

public class GuiProxy implements IGuiHandler {
	@Nullable
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return Invoke.serverValue(() -> removed(ID, player, world, x, y, z));
	}

	@GradleSideOnly(GradleSide.SERVER)
	public Object removed(int ID, EntityPlayer player, World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TEEnchanter) {
			return new EnchanterContainer(player.inventory, (TEEnchanter) te);
		} else if (te instanceof TEAnvil) {
			return new AnvilContainer(player.inventory, (TEAnvil) te);
		}
		return null;
	}

	@Nullable
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof TEEnchanter) {
			TEEnchanter containerTileEntity = (TEEnchanter) te;
			return new EnchanterGuiContainer(containerTileEntity, new EnchanterContainer(player.inventory, containerTileEntity));
		} else if (te instanceof TEAnvil) {
			TEAnvil containerTileEntity = (TEAnvil) te;
			return new AnvilGuiContainer(containerTileEntity, new AnvilContainer(player.inventory, containerTileEntity));
		}
		return null;
	}
}
