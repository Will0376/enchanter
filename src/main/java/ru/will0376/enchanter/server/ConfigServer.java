package ru.will0376.enchanter.server;


import ru.will0376.enchanter.common.Config;

import java.io.File;


public class ConfigServer extends Config {
	boolean disableRestoreItem;
	int oxyCoinProviderIndex;
	float multiple;

	public ConfigServer(File configFile) {
		super(configFile);
	}

	@Override
	public void setConfigs() {
		disableRestoreItem = getConfiguration().getBoolean("disableRestoreItem", GENERAL, false, "");
		oxyCoinProviderIndex = getConfiguration().getInt("oxyCoinProviderIndex", GENERAL, 0, 0, 127, "");
		multiple = getConfiguration().getFloat("multiple", GENERAL, 10, 0, Float.MAX_VALUE, "");
	}

	public boolean isDisableRestoreItem() {
		return disableRestoreItem;
	}

	public int getOxyCoinProviderIndex() {
		return oxyCoinProviderIndex;
	}

	public float getMultiple() {
		return multiple;
	}
}
