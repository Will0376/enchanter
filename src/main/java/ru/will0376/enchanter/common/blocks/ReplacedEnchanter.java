package ru.will0376.enchanter.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

public class ReplacedEnchanter extends Block implements ITileEntityProvider {
	public static final int GUI_ID = 1;
	protected static final AxisAlignedBB AABB = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);

	public ReplacedEnchanter() {
		super(Material.PORTAL);
		setTranslationKey(Enchanter.MOD_ID + ".replacedenchanter");
		setRegistryName("replacedenchanter");
		setCreativeTab(Enchanter.tab);
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}


	@SideOnly(Side.CLIENT)
	public void initModel() {
		Minecraft.getMinecraft()
				.getRenderItem()
				.getItemModelMesher()
				.register(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TEEnchanter();
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return AABB;
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (world.isRemote) {
			return true;
		}
		TileEntity te = world.getTileEntity(pos);
		if (!(te instanceof TEEnchanter)) {
			return false;
		}
		player.openGui(Enchanter.INSTANCE, GUI_ID, world, pos.getX(), pos.getY(), pos.getZ());
		return true;
	}
}
