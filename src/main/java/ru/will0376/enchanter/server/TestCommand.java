package ru.will0376.enchanter.server;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToClientDiscoverer;

import static ru.will0376.enchanter.common.net.ToClientDiscoverer.Action.Service;

@GradleSideOnly(GradleSide.SERVER)
public class TestCommand extends CommandBase {
	@Override
	public String getName() {
		return "enchanter";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length >= 1) {
			switch (args[0]) {
				case "checkBall":
					Net.get().sendTo(new ToClientDiscoverer(Service), (EntityPlayerMP) sender);
			}
		}
	}
}
