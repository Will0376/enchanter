package ru.will0376.enchanter.server;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.will0376.enchanter.common.CraftRegister;
import ru.will0376.enchanter.common.net.Net;

public class ServerProxy {
	public void preInit(FMLPreInitializationEvent event) {
		Net.init();
	}

	public void init(FMLInitializationEvent event) {
		CraftRegister.registerRecipes();
	}

	public void postInit(FMLPostInitializationEvent event) {
	}

}
