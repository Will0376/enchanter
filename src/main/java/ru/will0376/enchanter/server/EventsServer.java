package ru.will0376.enchanter.server;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToClientAnvil;

@GradleSideOnly(GradleSide.SERVER)
@Mod.EventBusSubscriber
public class EventsServer {

	@SubscribeEvent
	public static void login(PlayerEvent.PlayerLoggedInEvent event) {
		Net.get()
				.sendTo(new ToClientAnvil(ToClientAnvil.Action.setMultiple, Enchanter.config.getServerConfig()
						.getMultiple()), (EntityPlayerMP) event.player);
		Net.get()
				.sendTo(new ToClientAnvil(ToClientAnvil.Action.setIndex, Enchanter.config.getServerConfig()
						.getOxyCoinProviderIndex()), (EntityPlayerMP) event.player);
		PlayerPojo pcfg;
		Enchanter.playerCfgMap.put(event.player.getName(), pcfg = PlayerPojo.loadPlayerCfg(event.player.getName()));
		pcfg.sendToPlayer();
	}

	@SubscribeEvent
	public static void savePlayer(PlayerEvent.PlayerLoggedOutEvent event) {
		Enchanter.playerCfgMap.get(event.player.getName()).saveCfg();
		Enchanter.playerCfgMap.remove(event.player.getName());
	}

}
