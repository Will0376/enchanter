package ru.will0376.enchanter.server;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToClientDiscoverer;

import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class PlayerPojo {
	public static volatile File rootConfigDir;
	@Expose(serialize = false, deserialize = false)
	public static PlayerPojo clientpojo;
	@Expose
	private int openedlvl = 0;
	@Expose
	private String nick;

	@GradleSideOnly(GradleSide.SERVER)
	public static PlayerPojo loadPlayerCfg(String nick) {
		if (Enchanter.playerCfgMap.containsKey(nick)) return Enchanter.playerCfgMap.get(nick);
		File playerFile = new File(rootConfigDir, nick);
		if (!playerFile.exists()) {
			PlayerPojo pcfg;
			Enchanter.playerCfgMap.put(nick, pcfg = new PlayerPojo().setNick(nick));
			return pcfg;
		}
		try {
			return getInstance(new String(Files.readAllBytes(playerFile.toPath()), StandardCharsets.UTF_8));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void setClientpojo(String json) {
		clientpojo = getInstance(json);
	}

	public static PlayerPojo getInstance(String json) {
		return new GsonBuilder().create().fromJson(json, PlayerPojo.class);
	}

	@GradleSideOnly(GradleSide.SERVER)
	public PlayerPojo replaceInMap() {
		Enchanter.playerCfgMap.put(nick, this);
		return this;
	}

	public String getNick() {
		return nick;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public PlayerPojo setNick(String nick) {
		this.nick = nick;
		return this;
	}

	public int getOpenedlvl() {
		return openedlvl;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public PlayerPojo setOpenedlvl(int openedlvl) {
		this.openedlvl = openedlvl;
		return this;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public void sendToPlayer() {
		Net.get().sendTo(new ToClientDiscoverer(ToClientDiscoverer.Action.SetPojo, toString()), getPlayerMP());
	}

	@GradleSideOnly(GradleSide.SERVER)
	public EntityPlayerMP getPlayerMP() {
		for (EntityPlayerMP player : FMLCommonHandler.instance()
				.getMinecraftServerInstance()
				.getPlayerList()
				.getPlayers()) {
			if (player.getName().equals(nick)) return player;
		}
		return null;
	}

	public String toString() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
	}

	@GradleSideOnly(GradleSide.SERVER)
	public PlayerPojo saveCfg() {
		try {
			File playerFile = new File(rootConfigDir, nick);
			FileWriter fw = new FileWriter(playerFile);
			fw.write(toString());
			fw.flush();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

}

