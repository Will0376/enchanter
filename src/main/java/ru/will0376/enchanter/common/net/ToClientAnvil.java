package ru.will0376.enchanter.common.net;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.will0376.enchanter.Enchanter;

public class ToClientAnvil implements IMessage {
	Action action;
	float localmult;

	public ToClientAnvil() {
	}

	public ToClientAnvil(Action action, float localmult) {
		this.action = action;
		this.localmult = localmult;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		action = Action.getById(buf.readInt());
		localmult = buf.readFloat();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
		buf.writeFloat(localmult);
	}

	public enum Action {
		setMultiple,
		setIndex;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToClientAnvil, IMessage> {

		@Override
		public ToClientAnvil onMessage(ToClientAnvil message, MessageContext ctx) {
			if (FMLCommonHandler.instance().getSide().isClient()) switch (message.action) {
				case setMultiple:
					Enchanter.multiple = message.localmult;
					break;
				case setIndex:
					Enchanter.index = (int) message.localmult;
					break;
			}
			return null;
		}
	}
}
