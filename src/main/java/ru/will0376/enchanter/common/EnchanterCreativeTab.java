package ru.will0376.enchanter.common;

import net.minecraft.item.ItemStack;
import ru.will0376.enchanter.Enchanter;

public class EnchanterCreativeTab extends net.minecraft.creativetab.CreativeTabs {
	public EnchanterCreativeTab() {
		super("tab." + Enchanter.MOD_ID);
	}

	@Override
	public ItemStack createIcon() {
		return new ItemStack(ModBlock.replacedenchanter);
	}
}
