package ru.will0376.enchanter.common.net;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.will0376.enchanter.client.guicontainer.EnchanterGuiContainer;

import java.awt.*;

public class ToClientEnchanter implements IMessage {
	Action action;
	String text;
	float random;
	float chance;

	public ToClientEnchanter() {
	}

	public ToClientEnchanter(Action action, String text) {
		this.action = action;
		this.text = text;
		random = -1;
		chance = -1;
	}

	public ToClientEnchanter(Action action, String text, float random, float chance) {
		this.action = action;
		this.text = text;
		this.random = random;
		this.chance = chance;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		action = Action.getById(buf.readInt());
		text = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(action.ordinal());
		ByteBufUtils.writeUTF8String(buf, text);
	}

	public enum Action {
		ErrorMessage,
		Success,
		Fail;

		public static Action getById(int ordinal) {
			return values()[ordinal];
		}
	}

	public static class Handler implements IMessageHandler<ToClientEnchanter, IMessage> {

		@Override
		public ToClientEnchanter onMessage(ToClientEnchanter message, MessageContext ctx) {
			if (FMLCommonHandler.instance().getSide().isClient())
				if (Minecraft.getMinecraft().currentScreen instanceof EnchanterGuiContainer) {
					EnchanterGuiContainer gui = (EnchanterGuiContainer) Minecraft.getMinecraft().currentScreen;
					switch (message.action) {
						case ErrorMessage:
							gui.lastText = message.text;
							gui.color = Color.RED;
							gui.tickText = 20 * 6;
							break;
						case Success:
							gui.color = Color.GREEN;
							gui.lastText = message.text;
							gui.random = message.random;
							gui.lastChance = message.chance;
							gui.tickText = 20 * 6;
							break;
						case Fail:
							gui.lastText = message.text;
							gui.color = Color.RED;
							gui.random = message.random;
							gui.lastChance = message.chance;
							gui.tickText = 20 * 6;
							break;
					}
				}
			return null;
		}
	}
}
