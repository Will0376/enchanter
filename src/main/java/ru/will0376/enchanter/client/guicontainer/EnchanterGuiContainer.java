package ru.will0376.enchanter.client.guicontainer;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.container.EnchanterContainer;
import ru.will0376.enchanter.common.net.Net;
import ru.will0376.enchanter.common.net.ToServerEnchanter;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

import java.awt.*;
import java.io.IOException;

import static ru.will0376.enchanter.common.net.ToServerEnchanter.nbtEnchantLvlConstant;

public class EnchanterGuiContainer extends GuiContainer {
	public static final int WIDTH = 180;
	public static final int HEIGHT = 152;
	private static final ResourceLocation background = new ResourceLocation(Enchanter.MOD_ID, "textures/gui/enchantbg.png");
	public String lastText = "";
	public int tickText = 0;
	public Color color = Color.BLACK;
	public float random = -1;
	public float lastChance = -1;
	TEEnchanter tileEntity;
	private GuiButton button;

	public EnchanterGuiContainer(TEEnchanter tileEntity, EnchanterContainer container) {
		super(container);
		this.tileEntity = tileEntity;
		xSize = WIDTH;
		ySize = HEIGHT;
	}

	@Override
	public void initGui() {
		button = addButton(new Button(0, this.width / 2 - 50, this.height / 2 - 64, 100, 20, I18n.format("gui.button.enchant")));
		super.initGui();
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			Net.get().sendToServer(new ToServerEnchanter(ToServerEnchanter.Action.Enchant));
		}
		super.actionPerformed(button);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (tickText > 0)
			fontRenderer.drawString(lastText, width / 2 - this.fontRenderer.getStringWidth(lastText) / 2, height / 2 - 43, color
					.getRGB(), false);
		renderChance();
		renderHoveredToolTip(mouseX, mouseY);
	}

	private void renderChance() {

		ItemStack input = tileEntity.getItemStackHandler().getStackInSlot(0);
		ItemStack inputEnchant = tileEntity.getItemStackHandler().getStackInSlot(1);
		if (!input.isEmpty()) {

			boolean firstEnchant = true;
			if (input.hasTagCompound() && input.getTagCompound().hasKey(nbtEnchantLvlConstant)) firstEnchant = false;

			int curlvl = firstEnchant ? 0 : input.getTagCompound().getInteger(nbtEnchantLvlConstant);
			float chance = ToServerEnchanter.Handler.setChance(input, inputEnchant, curlvl, false);
			if (chance == -999) chance = 0;

			fontRenderer.drawString("Change: " + chance, width / 2 - 85, height / 2 - 20, Color.GREEN.getRGB(), true);
		}
	}

	@Override
	public void updateScreen() {
		super.updateScreen();
		if (tickText > 0) tickText--;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		mc.getTextureManager().bindTexture(background);
		drawTexturedModalRect(guiLeft + 1, guiTop - 14, 0, 0, xSize, ySize + 14);
	}
}
