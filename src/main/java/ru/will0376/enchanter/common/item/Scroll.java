package ru.will0376.enchanter.common.item;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import ru.will0376.enchanter.Enchanter;

import javax.annotation.Nullable;
import java.util.List;

public class Scroll extends Item {
	public Scroll() {
		super();
		this.setRegistryName(Enchanter.MOD_ID, "scroll");
		this.setTranslationKey("scroll");
		this.setCreativeTab(Enchanter.tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if (tab == Enchanter.tab) for (EnumScroll type : EnumScroll.values()) {
			ItemStack is = new ItemStack(this, 1, type.ordinal());
			is.setTranslatableName("scroll." + type.ordinal());
			items.add(is);
		}
	}

	@Override
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		int cur = stack.getMetadata();
		tooltip.add(I18n.format("scroll.item.tooltip", 10 + (cur * 5) + "%"));
	}

	@Override
	public Item setHasSubtypes(boolean hasSubtypes) {
		return super.setHasSubtypes(hasSubtypes);
	}

	public enum EnumScroll implements IStringSerializable {
		N0("0"),
		N1("1"),
		N2("2");


		private final String blockModelName;
		private final int id;

		EnumScroll(String blockModelName) {
			this.blockModelName = blockModelName;
			this.id = Integer.parseInt(blockModelName);
		}


		public static Scroll.EnumScroll getById(int id) {
			if (id < values().length) return values()[id];
			return N0;
		}

		@Override
		public String getName() {
			return blockModelName;
		}

		public int getId() {
			return id;
		}
	}
}
