package ru.will0376.enchanter.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.enchanter.Enchanter;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@SideOnly(Side.CLIENT)
public class GuiButtonSome extends GuiButton {
	public ResourceLocation img, imgSelected;
	public boolean selected;
	int h, w;

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, String buttonText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
		h = heightIn;
		w = widthIn;
		img = new ResourceLocation(Enchanter.MOD_ID + ":" + path);
		imgSelected = new ResourceLocation(Enchanter.MOD_ID + ":" + selectedPath);
	}

	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		if (this.visible) {
			mc.getTextureManager().bindTexture(img);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

			if (selected) mc.getTextureManager().bindTexture(imgSelected);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			draw();

			this.mouseDragged(mc, mouseX, mouseY);


		}
	}

	public void drawHover(int mouseX, int mouseY) {
		this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
		if (hovered) drawHoveringTexts(this.displayString, mouseX, mouseY, Minecraft.getMinecraft().fontRenderer);
	}

	private void draw() {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		bufferbuilder.pos(x, y + h, 0).tex(0, 1).endVertex();
		bufferbuilder.pos(x + w, y + h, 0).tex(1, 1).endVertex();
		bufferbuilder.pos(x + w, y, 0).tex(1, 0).endVertex();
		bufferbuilder.pos(x, y, 0).tex(0, 0).endVertex();
		tessellator.draw();
	}

	public GuiButtonSome setSelected(boolean selected) {
		this.selected = selected;
		return this;
	}

	public GuiButtonSome switchSelected() {
		return setSelected(!selected);
	}

	protected void drawHoveringTexts(String hoverText, double xCoord, double yCoord, FontRenderer font) {
		List<String> list = Collections.singletonList(hoverText);
		GlStateManager.disableRescaleNormal();
		GlStateManager.disableDepth();
		int k = 0;
		Iterator<String> iterator = list.iterator();
		int k2;
		while (iterator.hasNext()) {
			String j2 = iterator.next();
			k2 = font.getStringWidth(j2);
			if (k2 > k) k = k2;
		}
		int var15 = (int) (xCoord + 12);
		k2 = (int) (yCoord - 12);
		int i1 = 8;
		zLevel = 300;
		int j1 = -267386864;
		this.drawGradientRect(var15 - 3, k2 - 4, var15 + k + 3, k2 - 3, j1, j1);
		this.drawGradientRect(var15 - 3, k2 + i1 + 3, var15 + k + 3, k2 + i1 + 4, j1, j1);
		this.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 + i1 + 3, j1, j1);
		this.drawGradientRect(var15 - 4, k2 - 3, var15 - 3, k2 + i1 + 3, j1, j1);
		this.drawGradientRect(var15 + k + 3, k2 - 3, var15 + k + 4, k2 + i1 + 3, j1, j1);
		int k1 = 1347420415;
		int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
		this.drawGradientRect(var15 - 3, k2 - 3 + 1, var15 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
		this.drawGradientRect(var15 + k + 2, k2 - 3 + 1, var15 + k + 3, k2 + i1 + 3 - 1, k1, l1);
		this.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 - 3 + 1, k1, k1);
		this.drawGradientRect(var15 - 3, k2 + i1 + 2, var15 + k + 3, k2 + i1 + 3, l1, l1);
		for (String s1 : list) {
			font.drawStringWithShadow(s1, var15, k2, -1);
			k2 += 10;
		}
		zLevel = 0;
		GlStateManager.enableDepth();
		GlStateManager.enableRescaleNormal();
	}

}

