package ru.will0376.enchanter.common;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.blocks.Discoverer;
import ru.will0376.enchanter.common.blocks.ReplacedAnvil;
import ru.will0376.enchanter.common.blocks.ReplacedEnchanter;
import ru.will0376.enchanter.common.item.Ball;
import ru.will0376.enchanter.common.item.Scroll;

public class ModBlock {
	@GameRegistry.ObjectHolder(Enchanter.MOD_ID + ":replacedenchanter")
	public static ReplacedEnchanter replacedenchanter;

	@GameRegistry.ObjectHolder(Enchanter.MOD_ID + ":replacedanvil")
	public static ReplacedAnvil replacedAnvil;

	@GameRegistry.ObjectHolder(Enchanter.MOD_ID + ":discoverer")
	public static Discoverer discoverer;

	@GameRegistry.ObjectHolder(Enchanter.MOD_ID + ":ball")
	public static Ball ball;

	@GameRegistry.ObjectHolder(Enchanter.MOD_ID + ":scroll")
	public static Scroll scroll;


	@SideOnly(Side.CLIENT)
	public static void initModels() {
		replacedenchanter.initModel();
		replacedAnvil.initModel();
		discoverer.initModel();
	}
}
