package ru.will0376.enchanter.client;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import ru.will0376.enchanter.common.ModBlock;
import ru.will0376.enchanter.common.item.Ball;
import ru.will0376.enchanter.common.item.Scroll;

@Mod.EventBusSubscriber(Side.CLIENT)
public class EventsClient {
	//	private static boolean firstload = true;
//
//	@SubscribeEvent
//	public static void loadLastWorld(GuiOpenEvent event) {
//		Minecraft mc = Minecraft.getMinecraft();
//		if (Enchanter.debug && !firstload)
//			if (event.getGui() instanceof GuiMainMenu)
//				mc.displayGuiScreen(new GuiWorldSelection(event.getGui()));
//			else if (event.getGui() instanceof GuiWorldSelection) {
//				GuiListWorldSelection guiListWorldSelection = new GuiListWorldSelection((GuiWorldSelection) event.getGui(), mc, 100, 100, 32, 100 - 64, 36);
//				try {
//					guiListWorldSelection.getListEntry(0).joinWorld();
//
//				} catch (Exception ignore) {
//				} finally {
//					firstload = !firstload;
//				}
//			}
//	}
	@SubscribeEvent
	public static void registerMetaBlocks(ModelRegistryEvent e) {
		Item item = ModBlock.ball;
		for (Ball.EnumBall type : Ball.EnumBall.values())
			ModelLoader.setCustomModelResourceLocation(item, type.ordinal(), new ModelResourceLocation(item.getRegistryName() + "_" + type
					.getName(), "inventory"));

		item = ModBlock.scroll;
		for (Scroll.EnumScroll type : Scroll.EnumScroll.values())
			ModelLoader.setCustomModelResourceLocation(item, type.ordinal(), new ModelResourceLocation(item.getRegistryName() + "_" + type
					.getName(), "inventory"));

	}
}
