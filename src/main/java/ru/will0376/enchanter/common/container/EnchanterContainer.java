package ru.will0376.enchanter.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import ru.will0376.enchanter.common.container.slot.LapisSlot;
import ru.will0376.enchanter.common.container.slot.ReverceToolSlot;
import ru.will0376.enchanter.common.container.slot.ToolSlot;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

import javax.annotation.Nonnull;

public class EnchanterContainer extends Container {
	private final TEEnchanter te;

	public EnchanterContainer(IInventory playerInventory, TEEnchanter te) {
		this.te = te;
		addOwnSlots();
		addPlayerSlots(playerInventory);
	}


	private void addPlayerSlots(IInventory playerInventory) {
		for (int row = 0; row < 3; ++row) {
			for (int col = 0; col < 9; ++col) {
				int x = 9 + col * 18;
				int y = row * 18 + 70;
				this.addSlotToContainer(new Slot(playerInventory, col + row * 9 + 9, x, y));
			}
		}
		for (int row = 0; row < 9; ++row) {
			int x = 9 + row * 18;
			int y = 58 + 70;
			this.addSlotToContainer(new Slot(playerInventory, row, x, y));
		}
	}

	private void addOwnSlots() {
		IItemHandler itemHandler = this.te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
		int slotIndex = 0;
		addSlotToContainer(new ToolSlot(itemHandler, slotIndex++, 62, -7));
		addSlotToContainer(new ReverceToolSlot(itemHandler, slotIndex++, 99, -7));
		addSlotToContainer(new LapisSlot(itemHandler, slotIndex++, 153, 14));
		addSlotToContainer(new SlotItemHandler(itemHandler, slotIndex++, 81, 45) {
			@Override
			public boolean isItemValid(@Nonnull ItemStack stack) {
				return super.isItemValid(stack) && stack.getItem() != Items.DYE;
			}
		});
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index < TEEnchanter.SIZE) {
				if (!this.mergeItemStack(itemstack1, TEEnchanter.SIZE, this.inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, TEEnchanter.SIZE, false)) {
				return ItemStack.EMPTY;
			}

			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
		}

		return itemstack;
	}

	@Override
	public void onContainerClosed(EntityPlayer playerIn) {
		super.onContainerClosed(playerIn);
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return te.canInteractWith(playerIn);
	}
}
