package ru.will0376.enchanter;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.enchanter.common.Config;
import ru.will0376.enchanter.common.EnchanterCreativeTab;
import ru.will0376.enchanter.common.GuiProxy;
import ru.will0376.enchanter.server.PlayerPojo;
import ru.will0376.enchanter.server.ServerProxy;
import ru.will0376.enchanter.server.TestCommand;

import java.io.File;
import java.util.HashMap;

@Mod(modid = Enchanter.MOD_ID, name = Enchanter.MOD_NAME, version = Enchanter.VERSION)
public class Enchanter {

	public static final String MOD_ID = "enchanter";
	public static final String MOD_NAME = "Enchanter";
	public static final String VERSION = "@version@";
	public static boolean debug = true;
	@Mod.Instance(MOD_ID)
	public static Enchanter INSTANCE;
	public static Config config;
	@SidedProxy(clientSide = "ru.will0376.enchanter.client.ClientProxy", serverSide = "ru.will0376.enchanter.server.ServerProxy")
	public static ServerProxy proxy;
	public static CreativeTabs tab = new EnchanterCreativeTab();
	public static float multiple = 0;
	public static int index = 0;
	@GradleSideOnly(GradleSide.SERVER)
	public static HashMap<String, PlayerPojo> playerCfgMap;

	static {
		Invoke.server(() -> playerCfgMap = new HashMap<>());
	}

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (debug) debug = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
		config = Config.getNewConfig(event.getSuggestedConfigurationFile()).launch();
		Invoke.server(() -> {
			PlayerPojo.rootConfigDir = new File(event.getModConfigurationDirectory(), MOD_NAME);
			if (!PlayerPojo.rootConfigDir.exists()) PlayerPojo.rootConfigDir.mkdir();
		});
		proxy.preInit(event);

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(INSTANCE, new GuiProxy());
		proxy.init(event);
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}

	@Mod.EventHandler
	@GradleSideOnly(GradleSide.SERVER)
	public void serverStart(FMLServerStartingEvent event) {
		event.registerServerCommand(new TestCommand());
	}
}