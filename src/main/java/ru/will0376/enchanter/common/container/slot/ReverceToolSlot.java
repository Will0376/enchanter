package ru.will0376.enchanter.common.container.slot;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class ReverceToolSlot extends ToolSlot {


	public ReverceToolSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return !super.isItemValid(stack) && stack.getItem() != Items.DYE;
	}
}
