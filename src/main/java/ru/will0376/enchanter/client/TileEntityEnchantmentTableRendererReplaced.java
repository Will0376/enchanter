package ru.will0376.enchanter.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBook;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import ru.will0376.enchanter.Enchanter;
import ru.will0376.enchanter.common.tileentity.TEEnchanter;

public class TileEntityEnchantmentTableRendererReplaced extends TileEntitySpecialRenderer<TEEnchanter> {
	private static final ResourceLocation TEXTURE_BOOK = new ResourceLocation(Enchanter.MOD_ID, "textures/enchanting_table_book.png");
	private final ModelBook modelBook = new ModelBook();

	@Override
	public void render(TEEnchanter te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		try {
			GlStateManager.pushMatrix();
			GlStateManager.translate((float) x + 0.5F, (float) y + 0.75F, (float) z + 0.5F);
			float f = (float) te.tickCount + partialTicks;
			GlStateManager.translate(0.0F, 0.1F + MathHelper.sin(f * 0.1F) * 0.01F, 0.0F);
			float f1;

			for (f1 = te.bookRotation - te.bookRotationPrev; f1 >= (float) Math.PI; f1 -= ((float) Math.PI * 2F)) {
			}

			while (f1 < -(float) Math.PI) {
				f1 += ((float) Math.PI * 2F);
			}

			float f2 = te.bookRotationPrev + f1 * partialTicks;
			GlStateManager.rotate(-f2 * (180F / (float) Math.PI), 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(80.0F, 0.0F, 0.0F, 1.0F);
			Minecraft.getMinecraft().renderEngine.bindTexture(TEXTURE_BOOK);
			float f3 = te.pageFlipPrev + (te.pageFlip - te.pageFlipPrev) * partialTicks + 0.25F;
			float f4 = te.pageFlipPrev + (te.pageFlip - te.pageFlipPrev) * partialTicks + 0.75F;
			f3 = (f3 - (float) MathHelper.fastFloor(f3)) * 1.6F - 0.3F;
			f4 = (f4 - (float) MathHelper.fastFloor(f4)) * 1.6F - 0.3F;

			if (f3 < 0.0F) {
				f3 = 0.0F;
			}

			if (f4 < 0.0F) {
				f4 = 0.0F;
			}

			if (f3 > 1.0F) {
				f3 = 1.0F;
			}

			if (f4 > 1.0F) {
				f4 = 1.0F;
			}

			float f5 = te.bookSpreadPrev + (te.bookSpread - te.bookSpreadPrev) * partialTicks;
			GlStateManager.enableCull();
			this.modelBook.render(null, f, f3, f4, f5, 0.0F, 0.0625F);
			GlStateManager.popMatrix();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
