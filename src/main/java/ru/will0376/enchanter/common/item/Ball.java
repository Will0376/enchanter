package ru.will0376.enchanter.common.item;

import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.enchanter.Enchanter;

import javax.annotation.Nullable;
import java.util.List;

public class Ball extends Item implements Cloneable {
	public Ball() {
		super();
		this.setRegistryName(Enchanter.MOD_ID, "ball");
		this.setTranslationKey("ball");
		this.setCreativeTab(Enchanter.tab);
		this.setHasSubtypes(true);
	}

	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if (tab == Enchanter.tab) {
			for (EnumBall type : EnumBall.values()) {
				try {
					ItemStack is = new ItemStack(this, 1, type.ordinal());
					is.setTranslatableName("ball." + type.ordinal());
					items.add(is);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		int cur = stack.getMetadata();
		tooltip.add(TextFormatting.GOLD + "" + TextFormatting.UNDERLINE + "" + I18n.format("ball." + cur));
		tooltip.add(I18n.format("ball.item.tooltip", cur, cur + 1));
	}

	@Override
	public Item setHasSubtypes(boolean hasSubtypes) {
		return super.setHasSubtypes(hasSubtypes);
	}

	public enum EnumBall implements IStringSerializable {
		N0("0"),
		N1("1"),
		N2("2"),
		N3("3"),
		N4("4"),
		N5("5"),
		N6("6"),
		N7("7"),
		N8("8"),
		N9("9"),
		N10("10"),
		N11("11");


		private final String blockModelName;
		private final int id;

		EnumBall(String blockModelName) {
			this.blockModelName = blockModelName;
			this.id = Integer.parseInt(blockModelName);
		}

		public static EnumBall getById(int id) {
			if (id < values().length) return values()[id];
			return N0;
		}

		@Override
		public String getName() {
			return blockModelName;
		}

		public int getId() {
			return id;
		}
	}

}
